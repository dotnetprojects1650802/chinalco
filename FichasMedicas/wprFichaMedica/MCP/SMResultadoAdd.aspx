﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SMResultadoAdd.aspx.cs" Inherits="Chinalco.RRHH.Web.grpSalud.SM.SMResultadoAdd" %>

<!DOCTYPE html>

<html data-ng-app="auseApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SM Resultados Médicos</title>

    <link href="/Assets/css/bootstrap.min.css?ver=<%= DateTime.Now.Ticks %>" rel="stylesheet" />
    <link href="/Assets/css/bootstrap-dialog.min.css?ver=<%= DateTime.Now.Ticks %>" rel="stylesheet" />

    <link href="/Assets/css/jquery-ui.css?ver=<%= DateTime.Now.Ticks %>" rel="stylesheet" />
   
    <script src="/Assets/js/jquery-3.3.1.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="/Assets/js/angular-1.5.9.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="/Assets/js/bootstrap.min.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="/Assets/js/bootstrap-dialog.min.js?ver=<%= DateTime.Now.Ticks %>"></script>

    <script src="/Assets/js/jquery-ui.min.js?ver=<%= DateTime.Now.Ticks %>"></script>
      
    <script src="/Assets/js/MCPJs.js?ver=<%= DateTime.Now.Ticks %>"></script>

    
    <script src="../../js/GetFileBase64SM.js?ver=<%= DateTime.Now.Ticks %>"></script>

    <script type="text/javascript">
        var myApp = angular.module('auseApp', []);
        var mRootApi = "../api/";
        var oFileIns = {};
        var oFilePlacaF = {};
        var oFilePlacaL = {};

        oFileIns.Size = 0;
        oFileIns.Nombre = '';
        oFileIns.Data = '';

        oFilePlacaF.Size = 0;
        oFilePlacaF.Nombre = '';
        oFilePlacaF.Data = '';

        oFilePlacaL.Size = 0;
        oFilePlacaL.Nombre = '';
        oFilePlacaL.Data = '';
       
        myApp.controller('admPer', ['$scope', '$http', function ($scope, $http) {          
            $scope.lstData = [];
            $scope.Colaborador = JSON.parse($('#hdData').val());
            $scope.TO = {};
            $scope.TO.Key = 0;
            $scope.TO.KeyResultado = 0;
            $scope.TO.Observacion = '';
            $scope.TO.Fecha = '';
            $scope.TO.Colaborador = {};
            $scope.TO.TipoExamen = {};
            $scope.TO.TipoExamen.Key = '0';
            $scope.TO.TipoResultado = {};
            $scope.TO.TipoResultado.Key = '0';
            $scope.TO.Medico = {};
            $scope.TO.Medico.Key = '0';
            $scope.TO.Clinica = {};
            $scope.TO.Clinica.Key = '0';
            $scope.TO.EmpresaEvaluadora = {};
            $scope.TO.EmpresaEvaluadora.Key = '0';
            $scope.TO.FileCompleto = 'N';
            $scope.TO.TienePlaca = 'N';
            $scope.TO.TieneRestriccion = 'N';
            $scope.TO.Psicosensometrico = 'N';
            $scope.TO.TrabajoAltura = 'N';
            $scope.TO.Resonancia = 'N';
            $scope.TO.Anio = '';
            $scope.TO.Archivo = {};
            $scope.TO.PlacaF = {};
            $scope.TO.PlacaL = {};
                        
            $scope.TraerLista = function () {
                var xUrl = mRootApi + "WASaludResultado/ListarEvaluaciones/" +$('#hdPersonal').val();
              
                $http.get(xUrl).then(function (result) {
                    $scope.lstData = result.data;
                });
                               
            };

            $scope.Adicionar = function () {
                $scope.TO.Key = 0;
                $scope.TO.KeyResultado = 0;
                $scope.TO.Observacion = '';
                $scope.TO.Fecha = '';                            
                $scope.TO.TipoExamen.Key = '0';              
                $scope.TO.TipoResultado.Key = '0';              
                $scope.TO.Medico.Key = '0';              
                $scope.TO.Clinica.Key = '0';
                $scope.TO.EmpresaEvaluadora.Key = '0';
                $scope.TO.FileCompleto = 'N';
                $scope.TO.TienePlaca = 'N';
                $scope.TO.TieneRestriccion = 'N';
                $scope.TO.Psicosensometrico = 'N';
                $scope.TO.TrabajoAltura = 'N';
                $scope.TO.Resonancia = 'N';
                $scope.TO.Anio = '';
                $scope.TO.Archivo = {};
                $scope.TO.PlacaF = {};
                $scope.TO.PlacaL = {};

                oFileIns.Size = 0;
                oFileIns.Nombre = '';
                oFileIns.Data = '';

                oFilePlacaF.Size = 0;
                oFilePlacaF.Nombre = '';
                oFilePlacaF.Data = '';

                oFilePlacaL.Size = 0;
                oFilePlacaL.Nombre = '';
                oFilePlacaL.Data = '';

                $('#fupArchivo').val('');
                $('#fupPlacaF').val('');
                $('#fupPlacaL').val('');

                MostrarDialogo("popDatos");
            };

            $scope.Editar = function (pPos) {
                $scope.TO.Key = $scope.lstData[pPos].Key;
                $scope.TO.KeyResultado = $scope.lstData[pPos].KeyResultado;
                $scope.TO.Observacion = $scope.lstData[pPos].Observacion;
                $scope.TO.Fecha = $scope.lstData[pPos].Fecha;
                $scope.TO.TipoExamen.Key =String( $scope.lstData[pPos].TipoExamen.Key);
                $scope.TO.TipoResultado.Key =String( $scope.lstData[pPos].TipoResultado.Key);
                $scope.TO.Medico.Key =String( $scope.lstData[pPos].Medico.Key);
                $scope.TO.Clinica.Key =String( $scope.lstData[pPos].Clinica.Key);
                $scope.TO.EmpresaEvaluadora.Key =String( $scope.lstData[pPos].EmpresaEvaluadora.Key);
                $scope.TO.FileCompleto = $scope.lstData[pPos].FileCompleto;
                $scope.TO.TienePlaca = $scope.lstData[pPos].TienePlaca;
                $scope.TO.TieneRestriccion = $scope.lstData[pPos].TieneRestriccion;
                $scope.TO.Anio = $scope.lstData[pPos].Anio;
                $scope.TO.Psicosensometrico = $scope.lstData[pPos].Psicosensometrico;
                $scope.TO.TrabajoAltura = $scope.lstData[pPos].TrabajoAltura;
                $scope.TO.Resonancia = $scope.lstData[pPos].Resonancia;
                $scope.TO.Archivo = {};
                $scope.TO.PlacaF = {};
                $scope.TO.PlacaL = {};

                oFilePlacaF.Size = 0;
                oFilePlacaF.Nombre = '';
                oFilePlacaF.Data = '';

                oFilePlacaL.Size = 0;
                oFilePlacaL.Nombre = '';
                oFilePlacaL.Data = '';

                $('#fupArchivo').val('');
                $('#fupPlacaF').val('');
                $('#fupPlacaL').val('');

                MostrarDialogo("popDatos");
            };

            $scope.SavEvaluacion = function () {
                var xUrl = mRootApi + "WASaludResultado/RegistrarResultado/";

                $scope.TO.Colaborador = JSON.parse($('#hdData').val());               
                $scope.TO.Archivo.Size = oFileIns.Size;
                $scope.TO.Archivo.Nombre = oFileIns.Nombre;
                $scope.TO.Archivo.Data = oFileIns.Data;

                $scope.TO.PlacaF.Size = oFilePlacaF.Size;
                $scope.TO.PlacaF.Nombre = oFilePlacaF.Nombre;
                $scope.TO.PlacaF.Data = oFilePlacaF.Data;

                $scope.TO.PlacaL.Size = oFilePlacaL.Size;
                $scope.TO.PlacaL.Nombre = oFilePlacaL.Nombre;
                $scope.TO.PlacaL.Data = oFilePlacaL.Data;
               
                $http({
                    method: 'POST', url: xUrl,
                    data: JSON.stringify($scope.TO),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                }).then(function (result) {
                    var xData = result.data;

                    CerrarDialogo('popDatos');

                    $scope.TraerLista();
                },
                    function (error) {
                        MCPError2(error.data.Message);
                    });

            };

            $scope.ActualizarAnio = function () {
                var xFecha = $scope.TO.Fecha;

                if (xFecha.length > 9) {
                    $scope.TO.Anio = xFecha.substr(6, 4);
                }
            };

            $scope.VerExamen = function (pAdjunto) {
                var xUrl = mRootApi + "WASaludResultado/VerExamen/" + pAdjunto;

                AbrirDigital(xUrl);
            };

            $scope.VerPlacaFrontal = function (pAdjunto) {
                var xUrl = mRootApi + "WASaludResultado/VerPlacaFrontal/" + pAdjunto;

                AbrirDigital(xUrl);
            };

            $scope.VerPlacaLateral = function (pAdjunto) {
                var xUrl = mRootApi + "WASaludResultado/VerPlacaLateral/" + pAdjunto;

                AbrirDigital(xUrl);
            };

            $scope.ConfirmarBorrar = function (pKey) {
                
                var oPts = {
                    title: 'Confirmar operación',
                    message: "¿ Está seguro de borrar el elemento seleccionado ?",
                    btnCancelLabel: 'NO',
                    btnOKLabel: 'SI',
                    type: BootstrapDialog.TYPE_WARNING,
                    draggable: true, 
                    callback: function (result) { if (result) $scope.Borrar(pKey); }
                }
                
                BootstrapDialog.confirm(oPts );

            };

            $scope.Borrar = function (pKey) {
                var xUrl = mRootApi + "WASaludResultado/BorrarEvaluacion/"+pKey;
                              
                $http({
                    method: 'DELETE', url: xUrl,                   
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                }).then(function (result) {
                    var xData = result.data;
                
                    $scope.TraerLista();
                },
                    function (error) {
                        MCPError2(error.data.Message);
                    });

            };

            $scope.TraerLista();
          
        }]);

        function Regresar() {
            var xUrl = "SMResultados.aspx";

            NavegarA(xUrl);
        }
       
        $(function () {
        
           /* $('#txtFecha').datepicker();*/
            $('#txtFecha').datepicker({ maxDate: '0' });
            $('#txtFecha').datepicker("option", "dateFormat", "dd/mm/yy");
            
        });

    </script>

</head>
<body data-ng-controller="admPer" id="admPer">

<form id="form2" runat="server" autocomplete="off" enableviewstate="false">

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a class="navbar-brand" href="#" onclick="Regresar();" ><span class="glyphicon glyphicon-arrow-left"></span></a>
        <a class="navbar-brand" href="#">Resultados Médicos</a>
    </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

         <div class="navbar-right">
             <button type="button" class="btn btn-default navbar-btn" data-ng-click="Adicionar();">
                 Adicionar
             </button>
         </div>

      </div>

  </div>
</nav>

<div class="container-fluid">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Colaborador</label>
            <input id="Text1" type="text" class="form-control" data-ng-model="Colaborador.Nombre" disabled="disabled" />
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Empresa</label>
            <input id="Text2" type="text" class="form-control" data-ng-model="Colaborador.Empresa.Nombre" disabled="disabled" />
        </div>
    </div>
</div>

    <table class="table table-condensed bg-primary" style="margin-bottom: 0px; padding-bottom: 0px;">
        <thead>
            <tr>
                <td style="width: 08%">Fecha</td>
                <td style="width: 03%">Año</td>
                <td style="width: 15%">Tipo</td>
                <td style="width: 14%">Resultado</td>
                <td style="width: 21%">Emp.Evaluadora</td>
                <td style="width: 20%">Clinica</td>
                <td style="width: 03%;text-align:center">File</td>
                <td style="width: 05%;text-align:center">Placa F.</td>
                <td style="width: 05%;text-align:center">Placa L.</td>
                <td style="width: 06%;text-align:center"></td>
            </tr>
        </thead>
    </table>

    <table class="table table-condensed">
        <tr data-ng-repeat="x in lstData">
            <td style="width: 08%">{{ x.Fecha }}</td>
            <td style="width: 03%">{{ x.Anio }}</td>
            <td style="width: 15%">{{ x.TipoExamen.Nombre }}</td>
            <td style="width: 14%">{{ x.TipoResultado.Nombre }}</td>
            <td style="width: 21%">{{ x.EmpresaEvaluadora.Nombre }}</td>
            <td style="width: 20%">{{ x.Clinica.Nombre }}</td>            
            <td style="width: 03%;text-align:center">
                <a href="#" title="Ver Exámen" data-ng-click="VerExamen(x.Key);"><span class="glyphicon glyphicon-paperclip"></span></a>
            </td>                                      
            <td style="width: 05%;text-align:center">
                <a href="#" title="Ver Placa Frontal" data-ng-click="VerPlacaFrontal(x.Key);" data-ng-hide="x.PlacaF.Size==0"><span class="glyphicon glyphicon-paperclip"></span></a>
            </td>
            <td style="width: 05%;text-align:center">
                <a href="#" title="Ver Placa Lateral" data-ng-click="VerPlacaLateral(x.Key);" data-ng-hide="x.PlacaL.Size==0"><span class="glyphicon glyphicon-paperclip"></span></a>
            </td>
            <td style="width: 06%">
                <a href="#" title="Editar" data-ng-click="ConfirmarBorrar(x.Key);"><span class="glyphicon glyphicon-remove"></span></a>
                &nbsp;
                <a href="#" title="Editar" data-ng-click="Editar($index);"><span class="glyphicon glyphicon-pencil"></span></a>
            </td>
        </tr>
    </table>
   
    <asp:HiddenField ID="hdTieneAcceso" runat="server" Value="N" />
    <asp:HiddenField ID="hdPersonal" runat="server" Value="0" />
    <asp:HiddenField ID="hdData" runat="server" Value="" />

 <!-- modal Datos -->
<div class="modal" tabindex="-1" role="dialog" id="popDatos">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      
      <div class="modal-body">

        <div class="row">

            <div class="col-sm-6">

                <div class="form-group">
                    <label>Empresa Evaluadora</label>
                    <asp:DropDownList ID="cmbEmpEval" runat="server" CssClass="form-control" data-ng-model="TO.EmpresaEvaluadora.Key"></asp:DropDownList>
                </div>

                <div class="form-group">
                    <label>Clínica</label>
                    <asp:DropDownList ID="cmbClinica" runat="server" CssClass="form-control" data-ng-model="TO.Clinica.Key"></asp:DropDownList>
                </div>

                <div class="form-group">
                    <label>Médico</label>
                    <asp:DropDownList ID="cmbMedico" runat="server" CssClass="form-control" data-ng-model="TO.Medico.Key"></asp:DropDownList>                      
                </div>
        
                <div class="form-group">
                    <label>Tipo de Evaluación</label>
                    <asp:DropDownList ID="cmbTipoEvaluacion" runat="server" CssClass="form-control" data-ng-model="TO.TipoExamen.Key"></asp:DropDownList>
                </div>

                <div class="form-group">
                    <label>Resultado</label>
                    <asp:DropDownList ID="cmbResultado" runat="server" CssClass="form-control" data-ng-model="TO.TipoResultado.Key"></asp:DropDownList>
                </div>

                                                                
            </div>

            <div class="col-sm-3">

                <div class="form-group">
                    <label>Fecha Evaluación</label>
                    <input id="txtFecha" type="text" class="form-control" data-ng-model="TO.Fecha" data-ng-change="ActualizarAnio();" />
                </div>

                <div class="form-group">
                    <label>Año evaluación</label>
                    <input id="txtAnio" type="text" class="form-control" data-ng-model="TO.Anio" maxlength="4" onkeypress="SoloNros(event.keyCode);" />
                </div>

                <div class="form-group">
                    <label>Restricción para el puesto ?</label>
                    <select id="cmbTieneRest" class="form-control" data-ng-model="TO.TieneRestriccion">
                        <option value="S">Si</option>
                        <option value="N">No</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Psicosensométrico ?</label>
                    <select id="cmbPsicoseno" class="form-control" data-ng-model="TO.Psicosensometrico">
                        <option value="S">Si</option>
                        <option value="N">No</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Trab.Altura Estructural ?</label>
                    <select id="cmbTrabajo" class="form-control" data-ng-model="TO.TrabajoAltura">
                        <option value="S">Si</option>
                        <option value="N">No</option>
                    </select>
                </div>
                                                                                                
            </div>

            <div class="col-sm-3">
                
                <div class="form-group">
                    <label>Resonancia ?</label>
                    <select id="cmbResonancia" class="form-control" data-ng-model="TO.Resonancia">
                        <option value="S">Si</option>
                        <option value="N">No</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>File completo ?</label>
                    <select id="cmbFileCompleto" class="form-control" data-ng-model="TO.FileCompleto">
                        <option value="S">Si</option>
                        <option value="N">No</option>
                    </select>
                </div>
                
                <div class="form-group">
                    <label>Tiene placa radiográfica ?</label>
                    <select id="cmbTienePlaca" class="form-control" data-ng-model="TO.TienePlaca">
                        <option value="S">Si</option>
                        <option value="N">No</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Observaciones</label>
                    <textarea id="txtObservacion" cols="20" rows="2" class="form-control" data-ng-model="TO.Observacion"></textarea>
                </div>

                                                                                                                                                
            </div>

        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Archivo</label>
                    <input type="file" id="fupArchivo" onchange="GetBase64DesdeDigital('fupArchivo', oFileIns);" />
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Placa radiográfica frontal</label>
                    <input type="file" id="fupPlacaF" onchange="GetBase64DesdeDigital('fupPlacaF', oFilePlacaF);" />
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Placa radiográfica lateral</label>
                    <input type="file" id="fupPlacaL" onchange="GetBase64DesdeDigital('fupPlacaL', oFilePlacaL);" />
                </div>
            </div>
        </div>

        

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" data-ng-click="SavEvaluacion();">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<!-- Fin modal -->
 
</form>

</body>
</html>
