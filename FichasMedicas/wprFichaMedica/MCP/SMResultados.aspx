﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SMResultados.aspx.cs" Inherits="Chinalco.RRHH.Web.grpSalud.SM.SMResultados" %>

<!DOCTYPE html>

<html data-ng-app="auseApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SM Resultados Médicos</title>

    <link href="../css/bootstrap.min.css?ver=<%= DateTime.Now.Ticks %>" rel="stylesheet" />
    <link href="../css/bootstrap-dialog.min.css?ver=<%= DateTime.Now.Ticks %>" rel="stylesheet" />
   
    <script src="../js/jquery-3.3.1.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="../js/angular-1.5.9.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="../js/bootstrap.min.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="../js/bootstrap-dialog.min.js?ver=<%= DateTime.Now.Ticks %>"></script>
      
    <script src="../js/MCPJs.js?ver=<%= DateTime.Now.Ticks %>"></script>

    <script type="text/javascript">
        var myApp = angular.module('auseApp', []);
        var mRootApi = "../api/";
       
        myApp.controller('admPer', ['$scope', '$http', function ($scope, $http) {          
            $scope.lstData = [];          
                        
            $scope.TraerListaData = function () {
                var xUrl = mRootApi + "WASaludResultado/ListarResultados/" + $('#txtTexto').val();

                if ($('#txtTexto').val() == '') {
                    $('#txtTexto').focus();
                }
                else {

                    $http.get(xUrl).then(function (result) {
                        $scope.lstData = result.data;
                    });

                }
                
            };          

            $scope.VerDetalle = function (pColaborador, pEmpresa) {
                var xUrl = "SMResultadoAdd.aspx?KP=" + pColaborador+"&KE="+pEmpresa;

                NavegarA(xUrl);
                
            };
                               
            $scope.TraerListaData();
          
        }]);

        function TecEnter() {
            if (event.which == 13) {
                document.getElementById('btnBus').click();
                event.preventDefault();
            }
        }

       
        $(function () {

            $('#txtTexto').focus();
        });

    </script>

</head>
<body data-ng-controller="admPer" id="admPer">

<form id="form2" runat="server" autocomplete="off">

<nav class="navbar navbar-default" style="margin-bottom: 0;">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a class="navbar-brand" href="../" ><span class="glyphicon glyphicon-arrow-left"></span></a>
        <a class="navbar-brand" href="#">Resultados Médicos</a>
    </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

         <div class="navbar-right">
             <div class="navbar-form navbar-left">
                 <div class="form-group">
                     <div id="idTextBus" class="input-group">
                          <input name="txtTexto" type="text" id="txtTexto" class="form-control" placeholder="Texto de búsqueda" onkeypress="TecEnter();" />
                          <span class="input-group-btn">
                            <button id="btnBus" class="btn btn-primary" type="button" data-ng-click="TraerListaData();">Buscar</button>
                          </span>
                     </div>
                 </div>
             </div>

             <button type="button" class="btn btn-default navbar-btn" onclick="NavegarA('SMConfig.aspx');"><span class="glyphicon glyphicon-cog"></span> Configuración</button>

         </div>

      </div>

  </div>
</nav>

<table class="table table-condensed bg-primary" style="margin-bottom: 0px; padding-bottom: 0px;">
    <thead>
        <tr>            
            <td style="width: 30%">Colaborador</td>
            <td style="width: 25%">Empresa</td>
            <td style="width: 20%">Gerencia</td>
            <td style="width: 10%">F.Actualización</td>
            <td style="width: 10%">Registrado Por</td>
            <td style="width: 05%"></td>
        </tr>
    </thead>
</table>

<div style="width:100%; height:620px;overflow-y:auto; overflow-x:hidden; margin:0;padding:0;" >
    <table class="table table-condensed table-striped table-hover">
        <tr data-ng-repeat="x in lstData">
            <td style="width:30%">{{ x.Nombre }}</td>
            <td style="width:25%">{{ x.Empresa.Nombre }}</td>
            <td style="width:20%">{{ x.Gerencia }}</td> 
            <td style="width:10%">{{ x.Fecha }}</td>
            <td style="width:10%">{{ x.Usuario }}</td>
            <td style="width:05%;text-align:center;">
                <a href="#" data-ng-click="VerDetalle(x.Key, x.Empresa.Key);" title="Ver Detalle"><span class="glyphicon glyphicon-zoom-in"></span></a>               
            </td>
        </tr>
    </table>
</div>
   
    <asp:HiddenField ID="hdTieneAcceso" runat="server" Value="N" />

 
</form>

</body>
</html>
