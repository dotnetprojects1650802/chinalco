﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Chinalco.Shared;


using Chinalco.RRHH.Salud.BO;

namespace Chinalco.RRHH.Web.grpSalud.SM
{
    public partial class SMResultadoAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Inicializar();
        }

        private void Inicializar()
        {
           
            BOSaludTablas oBo = new BOSaludTablas();
           
            string xLogin = Comun.ObtenerLogin();
            int xPersonal = Comun.VerificarEntero(Comun.ObtenerQueryString(this, "KP", "0"));
            int xEmpresa = Comun.VerificarEntero(Comun.ObtenerQueryString(this, "KE", "0"));

            this.hdTieneAcceso.Value = "N";
            this.hdPersonal.Value = xPersonal.ToString();

            this.hdData.Value= Comun.SerializarToJSON( oBo.BuscarColaborador(xPersonal, xEmpresa ));
            this.hdTieneAcceso.Value = "S";

            Comun.WebComboAdapter(this.cmbMedico, oBo.ListarElementosCatalogo(3), "Key", "Nombre", true, "0");

            Comun.WebComboAdapter(this.cmbClinica, oBo.ListarElementosCatalogo(4), "Key", "Nombre", true, "0");

            Comun.WebComboAdapter(this.cmbTipoEvaluacion, oBo.ListarElementosCatalogo(1), "Key", "Nombre", true, "0");

            Comun.WebComboAdapter(this.cmbResultado, oBo.ListarElementosCatalogo(2), "Key", "Nombre", true, "0");

            Comun.WebComboAdapter(this.cmbEmpEval, oBo.ListarElementosCatalogo(5), "Key", "Nombre", true, "0");

          
        }

    }
}