﻿
using Chinalco.Shared;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Chinalco.RRHH.Web
{
  public class UtilsHttp
  {
    public static HttpResponseMessage RespuestaHttpPost(bool pResultado, Collection<TOError> pLstErrores)
    {
      HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
      HttpRequestMessage request = new HttpRequestMessage();
      JavaScriptSerializer scriptSerializer = new JavaScriptSerializer();
      HttpConfiguration configuration = new HttpConfiguration();
      TOResult toResult = new TOResult();
      string empty = string.Empty;
      request.SetConfiguration(configuration);
      HttpResponseMessage response;
      if (pResultado)
      {
        toResult.Codigo = 200;
        toResult.Valor = "OK";
        response = request.CreateResponse(HttpStatusCode.OK);
        string content = scriptSerializer.Serialize((object) toResult);
        response.Content = (HttpContent) new StringContent(content, Encoding.UTF8, "application/json");
      }
      else
      {
        if (pLstErrores.Count == 0)
          pLstErrores.Add(new TOError(400, "Error en la respuesta de WEBAPI"));
        string message = scriptSerializer.Serialize((object) pLstErrores);
        response = request.CreateResponse<HttpError>(HttpStatusCode.NotFound, new HttpError(message));
      }
      return response;
    }

    public static HttpResponseMessage RespuestaHttpPost(bool pResultado, Collection<TOError> pLstErrores, int pResult)
    {
      HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
      HttpRequestMessage request = new HttpRequestMessage();
      JavaScriptSerializer scriptSerializer = new JavaScriptSerializer();
      HttpConfiguration configuration = new HttpConfiguration();
      TOResult toResult = new TOResult();
      string empty = string.Empty;
      request.SetConfiguration(configuration);
      HttpResponseMessage response;
      if (pResultado)
      {
        toResult.Codigo = pResult;
        toResult.Valor = "OK";
        response = request.CreateResponse(HttpStatusCode.OK);
        string content = scriptSerializer.Serialize((object) toResult);
        response.Content = (HttpContent) new StringContent(content, Encoding.UTF8, "application/json");
      }
      else
      {
        if (pLstErrores.Count == 0)
          pLstErrores.Add(new TOError(400, "Error en la respuesta de WEBAPI"));
        string message = scriptSerializer.Serialize((object) pLstErrores);
        response = request.CreateResponse<HttpError>(HttpStatusCode.NotFound, new HttpError(message));
      }
      return response;
    }
  }
}
