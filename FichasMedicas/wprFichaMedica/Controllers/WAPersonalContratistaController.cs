﻿using System.Net.Http;
using System.Web.Http;
using Chinalco.RRHH.Salud.BO;
using Chinalco.RRHH.Salud.TO;
using Chinalco.Shared;

namespace Chinalco.RRHH.Web
{
    public class WAPersonalContratistaController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage ListarPersonalContratista(string pParametro)
        {
            BOPersonalContratista oBo = new BOPersonalContratista();

            return Comun.DevolverRespuestaJSON(Comun.SerializarToJSON(oBo.ListarPersonalContratista(pParametro)));
        } 

        [HttpPost]
        public HttpResponseMessage RegistrarPersonalContratista(TOPersonalContratista pParametro)
        {
            BOPersonalContratista oBo = new BOPersonalContratista();
            HttpResponseMessage oHttp;

            bool xRes = oBo.RegistrarPersonalContratista(pParametro, Comun.ObtenerLogin());
            oHttp = UtilsHttp.RespuestaHttpPost(xRes, oBo.Errores());

            return (oHttp);
        }
    }
}
