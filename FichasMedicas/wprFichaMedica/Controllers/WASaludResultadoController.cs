﻿
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

using Chinalco.RRHH.Salud.BO;
using Chinalco.RRHH.Salud.TO;
using Chinalco.Shared;

namespace Chinalco.RRHH.Web
{
  public class WASaludResultadoController : ApiController
  {
       
        [HttpGet]
        public HttpResponseMessage ListarResultados(string pParametro)
        {
            BOSaludResultado oBo = new BOSaludResultado();
            Collection<TOSaludResultado> oLista = new Collection<TOSaludResultado>();

            oLista = oBo.ListarResultados(pParametro);

            return Comun.DevolverRespuestaJSON( Comun.SerializarToJSON(oLista));
        }

        [HttpGet]
        public HttpResponseMessage ListarEvaluaciones(int pParametro)
        {
            BOSaludResultado oBo = new BOSaludResultado();
            Collection<TOSaludEvaluacion> oLista = new Collection<TOSaludEvaluacion>();

            oLista = oBo.ListarEvaluacionesPorPersona(pParametro);

            return Comun.DevolverRespuestaJSON(Comun.SerializarToJSON(oLista));
        }

        [HttpGet]
        public HttpResponseMessage ListarElementos(int pParametro)
        {
            BOSaludTablas oBo = new BOSaludTablas();
            Collection<TOSaludCatalogoItem> oLista = new Collection<TOSaludCatalogoItem>();

            oLista = oBo.ListarElementosCatalogo(pParametro);

            return Comun.DevolverRespuestaJSON(Comun.SerializarToJSON(oLista));
        }

        [HttpGet]
        public HttpResponseMessage VerExamen(int pParametro)
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            BOSaludResultado boAusentismo = new BOSaludResultado();
          
            try
            {
             
                TOBinario toBinario2 = boAusentismo.BuscarExamen(pParametro);
                httpResponseMessage.Content = (HttpContent)new StreamContent((Stream)new MemoryStream(toBinario2.Datos));
                httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(toBinario2.TipoMIME);
                httpResponseMessage.Content.Headers.ContentDisposition.FileName = toBinario2.Nombre.ToUpper();
                httpResponseMessage.Content.Headers.ContentLength = new long?((long)toBinario2.Datos.Length);
            }
            catch (Exception ex)
            {
                Comun.EventosGrabar("MCPRRHHWeb", "WASaludResultado.VerExamen", ex);
            }
            return httpResponseMessage;
        }

        [HttpGet]
        public HttpResponseMessage VerPlacaFrontal(int pParametro)
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            BOSaludResultado boAusentismo = new BOSaludResultado();

            try
            {

                TOBinario toBinario2 = boAusentismo.BuscarPlacaFrontal(pParametro);
                httpResponseMessage.Content = (HttpContent)new StreamContent((Stream)new MemoryStream(toBinario2.Datos));
                httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(toBinario2.TipoMIME);
                httpResponseMessage.Content.Headers.ContentDisposition.FileName = toBinario2.Nombre.ToUpper();
                httpResponseMessage.Content.Headers.ContentLength = new long?((long)toBinario2.Datos.Length);
            }
            catch (Exception ex)
            {
                Comun.EventosGrabar("MCPRRHHWeb", "WASaludResultado.VerPlacaFrontal", ex);
            }
            return httpResponseMessage;
        }

        [HttpGet]
        public HttpResponseMessage VerPlacaLateral(int pParametro)
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            BOSaludResultado boAusentismo = new BOSaludResultado();

            try
            {

                TOBinario toBinario2 = boAusentismo.BuscarPlacaLateral(pParametro);
                httpResponseMessage.Content = (HttpContent)new StreamContent((Stream)new MemoryStream(toBinario2.Datos));
                httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(toBinario2.TipoMIME);
                httpResponseMessage.Content.Headers.ContentDisposition.FileName = toBinario2.Nombre.ToUpper();
                httpResponseMessage.Content.Headers.ContentLength = new long?((long)toBinario2.Datos.Length);
            }
            catch (Exception ex)
            {
                Comun.EventosGrabar("MCPRRHHWeb", "WASaludResultado.VerPlacaLateral", ex);
            }
            return httpResponseMessage;
        }



        [HttpPost]
        public HttpResponseMessage RegistrarResultado(TOSaludEvaluacion pParametro)
        {
            BOSaludResultado oBo = new BOSaludResultado();
            HttpResponseMessage oHttp;
            bool xRes = false;

            xRes = oBo.RegistrarResultado(pParametro, Comun.ObtenerLogin());

            oHttp = UtilsHttp.RespuestaHttpPost(xRes, oBo.Errores());

            return (oHttp);
        }

        [HttpPost]
        public HttpResponseMessage RegistrarElemento(TOSaludCatalogoItem pParametro)
        {
            BOSaludTablas oBo = new BOSaludTablas();
            HttpResponseMessage oHttp;
            bool xRes = false;

            xRes = oBo.RegistrarElemento(pParametro, Comun.ObtenerLogin());

            oHttp = UtilsHttp.RespuestaHttpPost(xRes, oBo.Errores());

            return (oHttp);
        }

        
        [HttpDelete]
        public HttpResponseMessage BorrarEvaluacion(int pParametro)
        {
            BOSaludResultado oBo = new BOSaludResultado();
            HttpResponseMessage oHttp;
            bool xRes = false;

            xRes = oBo.BorrarEvaluacion(pParametro, Comun.ObtenerLogin());

            oHttp = UtilsHttp.RespuestaHttpPost(xRes, oBo.Errores());

            return (oHttp);
        }
    }
}
