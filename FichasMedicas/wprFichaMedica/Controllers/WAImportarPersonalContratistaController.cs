﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Chinalco.RRHH.Salud.BO;
using Chinalco.RRHH.Salud.TO;
using Chinalco.Shared;

namespace Chinalco.RRHH.Web
{
    public class WAImportarPersonalContratistaController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage RegistrarExcelPersonalContratista(TOImportarPersonalContratista pParametro)
        {
            BOExcelPersonalContratista oBo = new BOExcelPersonalContratista();
            HttpResponseMessage oHttp;

            bool xRes = oBo.RegistrarExcelPersonalContratista(pParametro, Comun.ObtenerLogin());
            oHttp = UtilsHttp.RespuestaHttpPost(xRes, oBo.Errores());

            return (oHttp);
        }

        [HttpGet]
        public HttpResponseMessage ListaDatosExcelNoProcesados()
        {
            BOExcelPersonalContratista oBo = new BOExcelPersonalContratista();

            return Comun.DevolverRespuestaJSON(Comun.SerializarToJSON(oBo.ListaDatosExcelNoProcesados()));
        }
    }
}
