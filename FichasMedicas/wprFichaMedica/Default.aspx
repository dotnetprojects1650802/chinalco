﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Chinalco.RRHH.Web.Default" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <title>MCP Fichas Medicas</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">


    <link href="css/bootstrap.min.css?ver=<%= DateTime.Now.Ticks %>" rel="stylesheet" />
    <link href="css/bootstrap-dialog.min.css?ver=<%= DateTime.Now.Ticks %>" rel="stylesheet" />

    <script src="js/jquery-3.3.1.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="js/angular-1.5.9.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="js/bootstrap.min.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="js/bootstrap-dialog.min.js?ver=<%= DateTime.Now.Ticks %>"></script>

    <script src="js/MCPJs.js?ver=<%= DateTime.Now.Ticks %>"></script>

    <script type="text/javascript">

        $(function () {
            if (history.forward(1)) {
                history.forward();
                location.href = document.referrer;
            }
        });

    </script>

</head>

<body>

    <nav class="navbar navbar-default" style="margin-bottom: 0;">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="#">Resultados Médicos</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="#" onclick="NavegarA('MCP/SMResultados.aspx');">Fichas Médicas MCP</a></li>
                        <li><a href="#" onclick="NavegarA('Contratistas/SMPersonalContratista.aspx');">Fichas Médicas Contratistas</a></li>
                        <li><a href="#" onclick="NavegarA('Contratistas/SMImportarPersonalContratista.aspx');">Importar Personal Contratista</a></li>
                    </ul>
                </div>

            </div>

        </div>
    </nav>



</body>
</html>

