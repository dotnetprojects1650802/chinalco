﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SMImportarPersonalContratista.aspx.cs"
    Inherits="Chinalco.RRHH.Web.grpSalud.SM.SMImportarPersonalContratista" %>

<!DOCTYPE html>

<html data-ng-app="auseApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Importar Personal Contratista</title>

    <link href="../css/bootstrap.min.css?ver=<%= DateTime.Now.Ticks %>" rel="stylesheet" />
    <link href="../css/bootstrap-dialog.min.css?ver=<%= DateTime.Now.Ticks %>" rel="stylesheet" />

    <script src="../js/jquery-3.3.1.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="../js/angular-1.5.9.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="../js/bootstrap.min.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="../js/bootstrap-dialog.min.js?ver=<%= DateTime.Now.Ticks %>"></script>

    <script src="../js/MCPJs.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="../js/GetFileBase64.js?ver=<%= DateTime.Now.Ticks %>"></script>

    <style>
        /* Agrega estilos a tu spinner */
        .spinner-container {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 9999;
        }

        .spinner {
            border: 6px solid rgba(0, 0, 0, 0.3);
            border-top: 6px solid #3498db;
            border-radius: 50%;
            width: 40px;
            height: 40px;
            animation: spin 1s linear infinite;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>

    <script type="text/javascript">
        var myApp = angular.module('auseApp', []);
        var mRootApi = "../api/";
        var oFilePersonalContratista = {};

        oFilePersonalContratista.Size = 0;
        oFilePersonalContratista.Nombre = '';
        oFilePersonalContratista.Data = '';

        myApp.controller('admPer', ['$scope', '$http', function ($scope, $http) {
            $scope.lstData = [];
            $scope.TO = {};
            $scope.TO.ExcelPersonalContratista = {};

            $scope.LoaderCargando = false;
            $scope.HabilitarBotonImportar = true;
            $scope.HabilitarBotonSubir = true;

            $scope.ListarDatosSinProcesar = function () {
                var xUrl = mRootApi + "WAImportarPersonalContratista/ListaDatosExcelNoProcesados/";

                $http.get(xUrl).then(function (result) {
                    $scope.lstData = result.data;
                });
            };

            $scope.RegistrarExcel = function () {
                var xUrl = mRootApi + "WAImportarPersonalContratista/RegistrarExcelPersonalContratista/";

                $scope.TO.ExcelPersonalContratista.Size = oFilePersonalContratista.Size;
                $scope.TO.ExcelPersonalContratista.Nombre = oFilePersonalContratista.Nombre;
                $scope.TO.ExcelPersonalContratista.Data = oFilePersonalContratista.Data;

                $scope.LoaderCargando = true;
                $scope.HabilitarBotonImportar = false;
                $scope.HabilitarBotonSubir = false;

                $http({
                    method: 'POST', url: xUrl,
                    data: JSON.stringify($scope.TO),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                }).then(function (result) {

                    $scope.ListarDatosSinProcesar();

                    $scope.LoaderCargando = false;
                    $scope.HabilitarBotonImportar = true;
                    $scope.HabilitarBotonSubir = true;

                    $('#fupPersonalContratista').val('');

                }, function (error) {
                    MCPError2(error.data.Message);
                    $scope.LoaderCargando = false;
                    $scope.HabilitarBotonImportar = true;
                    $scope.HabilitarBotonSubir = true;
                });
            };
        }]);

    </script>
</head>
<body data-ng-controller="admPer" id="admPer">
    <form id="form" runat="server" autocomplete="off">
        <!-- Inicio barra de navegación-->
        <nav class="navbar navbar-default" style="margin-bottom: 0;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../"><span class="glyphicon glyphicon-arrow-left"></span></a>
                    <a class="navbar-brand" href="#">Resultados Médicos</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="navbar-right">
                        <div class="navbar-form navbar-left">
                            <div class="form-group">
                                <div id="idTextBus" class="input-group">
                                    <input type="file" class="form-control-file navbar-btn" id="fupPersonalContratista" accept=".xlsx, .xls"
                                        onchange="GetBase64DesdeXLS('fupPersonalContratista', oFilePersonalContratista);" ng-disabled="!HabilitarBotonSubir" />
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary navbar-btn text-center" data-ng-click="RegistrarExcel();" ng-disabled="!HabilitarBotonImportar"><span class="glyphicon glyphicon-upload"></span>&nbsp;Importar</button>
                    </div>
                </div>
            </div>
        </nav>
        <!-- Fin barra de navegación-->
        <div class="form-group">
            <div class="spinner-container" ng-show="LoaderCargando">
                <div class="spinner"></div>
            </div>
        </div>
        <!-- Inicio Tabla Datos -->
        <table class="table table-condensed bg-primary" style="margin-bottom: 0px; padding-bottom: 0px;">
            <thead>
                <tr>
                    <td style="width: 5%">Fila</td>
                    <td style="width: 45%">Empresa</td>
                    <td style="width: 10%">Tipo Documento</td>
                    <td style="width: 10%">Numero Documento</td>
                    <td style="width: 30%">Nombre</td>
                </tr>
            </thead>
        </table>
        <div style="width: 100%; height: 620px; overflow-y: auto; overflow-x: hidden; margin: 0; padding: 0;">
            <table class="table table-condensed table-striped table-hover">
                <tr data-ng-repeat="x in lstData">
                    <td style="width: 5%">{{ x.Index }}</td>
                    <td style="width: 45%">{{ x.Empresa }}</td>
                    <td style="width: 10%">{{ x.TipoDOI }}</td>
                    <td style="width: 10%; text-align: center">{{ x.NumeroDOI }}</td>
                    <td style="width: 30%">{{ x.Nombre }}</td>
                </tr>
            </table>
        </div>
        <!-- Fin Tabla Datos -->
    </form>
</body>
</html>
