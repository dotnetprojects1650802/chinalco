﻿using System;
using System.Web.UI;
using Chinalco.RRHH.Salud.BO;
using Chinalco.Shared;

namespace Chinalco.RRHH.Web.grpSalud.SM
{
    public partial class SMPersonalContratista : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Inicializar();
        }
        private void Inicializar()
        {
            BOPersonalContratista oBo = new BOPersonalContratista();
            Comun.WebComboAdapter(cmbEmpresa, oBo.ListarEmpresas(), "Key", "Nombre", true, "0");
            Comun.WebComboAdapter(cmbTipoDocumento, oBo.ListarTiposDocumento(), "Key", "Nombre", true, "0");
        }
    }
}