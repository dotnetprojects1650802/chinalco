﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SMPersonalContratista.aspx.cs"
    Inherits="Chinalco.RRHH.Web.grpSalud.SM.SMPersonalContratista" %>

<!DOCTYPE html>

<html data-ng-app="auseApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Datos Personal Contratista</title>

    <link href="../css/bootstrap.min.css?ver=<%= DateTime.Now.Ticks %>" rel="stylesheet" />
    <link href="../css/bootstrap-dialog.min.css?ver=<%= DateTime.Now.Ticks %>" rel="stylesheet" />

    <script src="../js/jquery-3.3.1.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="../js/angular-1.5.9.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="../js/bootstrap.min.js?ver=<%= DateTime.Now.Ticks %>"></script>
    <script src="../js/bootstrap-dialog.min.js?ver=<%= DateTime.Now.Ticks %>"></script>

    <script src="../js/MCPJs.js?ver=<%= DateTime.Now.Ticks %>"></script>

    <script type="text/javascript">
        var myApp = angular.module('auseApp', []);
        var mRootApi = "../api/";

        myApp.controller('admPer', ['$scope', '$http', function ($scope, $http) {
            $scope.lstData = [];
            $scope.TO = {};

            $scope.TO.EstadoOperacion = 'add';

            $scope.TO.Contratista = {};
            $scope.TO.Contratista.Key = 0;

            $scope.TO.Empresa = {};
            $scope.TO.Empresa.Key = '0';

            $scope.TO.TipoDocumento = {};
            $scope.TO.TipoDocumento.Key = '0';

            $scope.TO.NumeroDocumento = '';
            $scope.TO.ApellidoPaterno = '';
            $scope.TO.ApellidoMaterno = '';
            $scope.TO.PrimerNombre = '';
            $scope.TO.SegundoNombre = '';

            $scope.ListarDatos = function () {
                var xUrl = mRootApi + "WAPersonalContratista/ListarPersonalContratista/" + $('#txtTexto').val();

                if ($('#txtTexto').val() == '') {
                    $('#txtTexto').focus();
                }
                else {
                    $http.get(xUrl).then(function (result) {
                        $scope.lstData = result.data;
                    });
                }
            };

            $scope.MostrarModal = function () {
                MostrarDialogo('popFormulario');
            };

            $scope.EditarDatos = function (pPos) {
                $scope.TO.EstadoOperacion = 'update';

                $scope.TO.Contratista.Key = $scope.lstData[pPos].Contratista.Key;

                $scope.TO.Empresa.Key = String($scope.lstData[pPos].Empresa.Key);
                $scope.TO.TipoDocumento.Key = String($scope.lstData[pPos].TipoDocumento.Key);

                $scope.TO.NumeroDocumento = $scope.lstData[pPos].NumeroDocumento;
                $scope.TO.ApellidoPaterno = $scope.lstData[pPos].ApellidoPaterno;
                $scope.TO.ApellidoMaterno = $scope.lstData[pPos].ApellidoMaterno;
                $scope.TO.PrimerNombre = $scope.lstData[pPos].PrimerNombre;
                $scope.TO.SegundoNombre = $scope.lstData[pPos].SegundoNombre;

                MostrarDialogo("popFormulario");
            };

            $scope.GuardarDatos = function () {
                var xUrl = mRootApi + "WAPersonalContratista/RegistrarPersonalContratista/";
                var nuevoRegistro = $scope.TO.PrimerNombre;

                $http({
                    method: 'POST',
                    url: xUrl,
                    data: JSON.stringify($scope.TO),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                }).then(function () {
                    $scope.LimpiarDatos();
                    $('#txtTexto').val(nuevoRegistro);
                    $scope.ListarDatos();
                    CerrarDialogo('popFormulario');
                },
                    function (error) {
                        MCPError2(error.data.Message);
                    });
            };

            $scope.LimpiarDatos = function () {
                $scope.TO.Contratista.Key = 0;
                $scope.TO.Empresa.Key = '0';
                $scope.TO.TipoDocumento.Key = '0';

                $scope.TO.EstadoOperacion = 'add';

                $scope.TO.NumeroDocumento = '';
                $scope.TO.ApellidoPaterno = '';
                $scope.TO.ApellidoMaterno = '';
                $scope.TO.PrimerNombre = '';
                $scope.TO.SegundoNombre = '';
            };

            $scope.ListarDatos();

        }]);

        function TecEnter() {
            if (event.which == 13) {
                document.getElementById('btnBus').click();
                event.preventDefault();
            }
        };

        $(function () {
            $('#txtTexto').focus();
        });

    </script>
</head>
<body data-ng-controller="admPer" id="admPer">
    <form id="form" runat="server" autocomplete="off">
        <!-- Inicio barra de navegación-->
        <nav class="navbar navbar-default" style="margin-bottom: 0;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../"><span class="glyphicon glyphicon-arrow-left"></span></a>
                    <a class="navbar-brand" href="#">Resultados Médicos</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="navbar-right">
                        <div class="navbar-form navbar-left">
                            <div class="form-group">
                                <div id="idTextBus" class="input-group">
                                    <input name="txtTexto" type="text" id="txtTexto" class="form-control" placeholder="Texto de búsqueda" onkeypress="TecEnter();" />
                                    <span class="input-group-btn">
                                        <button id="btnBus" class="btn btn-primary" type="button" data-ng-click="ListarDatos();"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary navbar-btn" data-ng-click="MostrarModal();"><span class="glyphicon glyphicon-plus"></span>&nbsp;Adicionar</button>
                    </div>
                </div>
            </div>
        </nav>
        <!-- Fin barra de navegación-->
        <!-- Inicio Tabla Datos -->
        <table class="table table-condensed bg-primary" style="margin-bottom: 0px; padding-bottom: 0px;">
            <thead>
                <tr>
                    <td style="width: 30%">Colaborador</td>
                    <td style="width: 25%">Empresa</td>
                    <td style="width: 6%; text-align: center"></td>
                </tr>
            </thead>
        </table>
        <div style="width: 100%; height: 620px; overflow-y: auto; overflow-x: hidden; margin: 0; padding: 0;">
            <table class="table table-condensed table-striped table-hover">
                <tr data-ng-repeat="x in lstData">
                    <td style="width: 30%">{{ x.Contratista.Nombre }}</td>
                    <td style="width: 25%">{{ x.Empresa.Nombre }}</td>
                    <td style="width: 06%">
                        <a href="#" title="EditarDatos" data-ng-click="EditarDatos($index);"><span class="glyphicon glyphicon-pencil"></span></a>
                    </td>
                </tr>
            </table>
        </div>
        <!-- Fin Tabla Datos -->
        <!-- Inicio modal Datos Formulario-->
        <div class="modal" tabindex="-1" role="dialog" id="popFormulario">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-footer">
                        <h3 class="text-center">Adicionar Personal Contratista</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="tipoDocumento">Empresa:</label>
                            <asp:DropDownList ID="cmbEmpresa" runat="server" CssClass="form-control" data-ng-model="TO.Empresa.Key"></asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label for="tipoDocumento">Tipo de Documento:</label>
                            <asp:DropDownList ID="cmbTipoDocumento" runat="server" CssClass="form-control" data-ng-model="TO.TipoDocumento.Key"></asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label for="numeroDocumento">Número de Documento:</label>
                            <input type="text" maxlength="20" class="form-control" id="numeroDocumento" data-ng-model="TO.NumeroDocumento" placeholder="Ingrese el número de documento">
                        </div>
                        <div class="form-group">
                            <label for="apellidoPaterno">Apellido Paterno:</label>
                            <input type="text" maxlength="50" class="form-control" id="apellidoPaterno" data-ng-model="TO.ApellidoPaterno" placeholder="Ingrese el apellido paterno">
                        </div>
                        <div class="form-group">
                            <label for="apellidoMaterno">Apellido Materno:</label>
                            <input type="text" maxlength="50" class="form-control" id="apellidoMaterno" data-ng-model="TO.ApellidoMaterno" placeholder="Ingrese el apellido materno">
                        </div>
                        <div class="form-group">
                            <label for="primerNombre">Primer Nombre:</label>
                            <input type="text" maxlength="50" class="form-control" id="primerNombre" data-ng-model="TO.PrimerNombre" placeholder="Ingrese el primer nombre">
                        </div>
                        <div class="form-group">
                            <label for="segundoNombre">Segundo Nombre:</label>
                            <input type="text" maxlength="50" class="form-control" id="segundoNombre" data-ng-model="TO.SegundoNombre" placeholder="Ingrese el segundo nombre">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-ng-click="LimpiarDatos();" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" data-ng-click="GuardarDatos();">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin modal Datos Formulario-->
    </form>
</body>
</html>
