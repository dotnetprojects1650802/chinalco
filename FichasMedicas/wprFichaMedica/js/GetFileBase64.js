﻿
function NombreDelArchivo(pInputFile) {
    var xFile = '#' + pInputFile;
    var xRes = false;
    var xExt = '';

    xFile = $(xFile).val();

    fileName = xFile.split('\\');
    xExt = fileName[fileName.length - 1];

    return (xExt)
}

function ExtensionDelArchivo(pFile) {
    var xFile = '#' + pFile;
    var xRes = false;
    var xExt = '';
    xFile = $(xFile).val();

    fileName = xFile.split('\\');
    fileName = fileName[fileName.length - 1];
    extention = fileName.split('.');

    xExt = extention[extention.length - 1];

    return (xExt)
}

function CheckArchivoExtension(pExt) {
    var xF = false;

    switch (pExt.toUpperCase()) {
        case 'PDF':
            xF = true;
            break;
        case 'JPG':
            xF = true;
            break;
        case 'JPEG':
            xF = true;
            break;
        case 'PNG':
            xF = true;
            break;
        case 'GIF':
            xF = true;
            break;
        case 'BMP':
            xF = true;
            break;
        case 'MSG':
            xF = true;
            break;
        case 'DOC':
            xF = true;
            break;
        case 'DOCX':
            xF = true;
            break;
        case 'XLS':
            xF = true;
            break;
        case 'XLSX':
            xF = true;
            break;

    }

    return (xF);
}

function CheckEsExcel(pExt) {
    var xF = false;

    switch (pExt.toUpperCase()) {
        case 'XLSX':
            xF = true;
            break;
    }

    return (xF);
}

function CheckEsZIP(pExt) {
    var xF = false;

    switch (pExt.toUpperCase()) {
        case 'ZIP':
            xF = true;
            break;
    }

    return (xF);
}

function CheckEsTexto(pExt) {
    var xF = false;

    switch (pExt.toUpperCase()) {
        case 'TXT':
            xF = true;
            break;
        case 'SQL':
            xF = true;
            break;
    }

    return (xF);
}

function CheckEsPDF(pExt) {
    var xF = false;

    switch (pExt.toUpperCase()) {
        case 'PDF':
            xF = true;
            break;        
    }

    return (xF);
}

function CheckEsCERT(pExt) {
    var xF = false;

    switch (pExt.toUpperCase()) {
        case 'PFX':
            xF = true;
            break;  
	case 'P12':
            xF = true;
            break;      
    }

    return (xF);
}

function CheckEsImagen(pExt) {
    var xF = false;

    switch (pExt.toUpperCase()) {
         case 'JPG':
            xF = true;
            break;
        case 'JPEG':
            xF = true;
            break;
        case 'PNG':
            xF = true;
            break;
        case 'GIF':
            xF = true;
            break;
        case 'BMP':
            xF = true;
            break; 
    }

    return (xF);
}


function GetBase64DesdeDigital(pControlHtml, pObjetoFile) {
    var reader2 = new FileReader();
    var fsize = 0;
    var xExt = '';
    var xNom = '';
    var xF = false;
    var errLst = $('<ol></ol>');
    var xControlHtml = '#' + pControlHtml;
  
    pObjetoFile.Size = 0;
    pObjetoFile.Nombre = '';
    pObjetoFile.Data = '';

    if ($(xControlHtml)[0].files.length > 0) {
        fsize = $(xControlHtml)[0].files[0].size;
        xExt = ExtensionDelArchivo(pControlHtml);
        xNom = NombreDelArchivo(pControlHtml);

        if (!CheckArchivoExtension(xExt)) {
            errLst.append($("<li />").text('Tipo de Archivo digital ' + xExt.toUpperCase() + ', no válido.'));
            xF = true;
        }

        if (fsize > 8192000) {
            errLst.append($("<li />").text("El tamaño del Archivo digital debe ser menor a 8 Mb."));
            xF = true;
        }

        if (xF) {
            pObjetoFile.Size = 0;
            pObjetoFile.Nombre = '';
            pObjetoFile.Data = '';
            $(xControlHtml).val('');
            MCPError(errLst);
        }
        else {
            pObjetoFile.Size = fsize;
            pObjetoFile.Nombre = xNom;

            reader2.readAsDataURL($(xControlHtml)[0].files[0]);
            reader2.onloadend = function () {

                pObjetoFile.Data = reader2.result;
            };
            reader2.onerror = function (error) {

                pObjetoFile.Size = 0;
                pObjetoFile.Nombre = '';
                pObjetoFile.Data = '';
                $(xControlHtml).val('');
                errLst.append($("<li />").text(error));
                MCPError(errLst);
            };
        }
       
    }

}

function GetBase64DesdeXLS(pControlHtml, pObjetoFile) {
    var reader2 = new FileReader();
    var fsize = 0;
    var xExt = '';
    var xNom = '';
    var xF = false;
    var errLst = $('<ol></ol>');
    var xControlHtml = '#' + pControlHtml;

    pObjetoFile.Size = 0;
    pObjetoFile.Nombre = '';
    pObjetoFile.Data = '';

    if ($(xControlHtml)[0].files.length > 0) {
        fsize = $(xControlHtml)[0].files[0].size;
        xExt = ExtensionDelArchivo(pControlHtml);
        xNom = NombreDelArchivo(pControlHtml);

        if (!CheckEsExcel(xExt)) {
            errLst.append($("<li />").text('Tipo de Archivo digital ' + xExt.toUpperCase() + ', no válido.'));
            xF = true;
        }

        if (fsize > 8192000) {
            errLst.append($("<li />").text("El tamaño del Archivo digital debe ser menor a 8 Mb."));
            xF = true;
        }

        if (xF) {
            pObjetoFile.Size = 0;
            pObjetoFile.Nombre = '';
            pObjetoFile.Data = '';
            $(xControlHtml).val('');
            MCPError(errLst);
        }
        else {
            pObjetoFile.Size = fsize;
            pObjetoFile.Nombre = xNom;

            reader2.readAsDataURL($(xControlHtml)[0].files[0]);
            reader2.onloadend = function () {

                pObjetoFile.Data = reader2.result;
            };
            reader2.onerror = function (error) {

                pObjetoFile.Size = 0;
                pObjetoFile.Nombre = '';
                pObjetoFile.Data = '';
                $(xControlHtml).val('');
                errLst.append($("<li />").text(error));
                MCPError(errLst);
            };
        }

    }

}

function GetBase64DesdeZIP(pControlHtml, pObjetoFile) {
    var reader2 = new FileReader();
    var fsize = 0;
    var xExt = '';
    var xNom = '';
    var xF = false;
    var errLst = $('<ol></ol>');
    var xControlHtml = '#' + pControlHtml;

    pObjetoFile.Size = 0;
    pObjetoFile.Nombre = '';
    pObjetoFile.Data = '';

    if ($(xControlHtml)[0].files.length > 0) {
        fsize = $(xControlHtml)[0].files[0].size;
        xExt = ExtensionDelArchivo(pControlHtml);
        xNom = NombreDelArchivo(pControlHtml);

        if (!CheckEsZIP(xExt)) {
            errLst.append($("<li />").text('Tipo de Archivo digital ' + xExt.toUpperCase() + ', no válido.'));
            xF = true;
        }

        if (fsize > 157286400) {
            errLst.append($("<li />").text("El tamaño del Archivo digital debe ser menor a 150 MB."));
            xF = true;
        }

        if (xF) {
            pObjetoFile.Size = 0;
            pObjetoFile.Nombre = '';
            pObjetoFile.Data = '';
            $(xControlHtml).val('');
            MCPError(errLst);
        }
        else {
            pObjetoFile.Size = fsize;
            pObjetoFile.Nombre = xNom;

            reader2.readAsDataURL($(xControlHtml)[0].files[0]);
            reader2.onloadend = function () {

                pObjetoFile.Data = reader2.result;
            };
            reader2.onerror = function (error) {

                pObjetoFile.Size = 0;
                pObjetoFile.Nombre = '';
                pObjetoFile.Data = '';
                $(xControlHtml).val('');
                errLst.append($("<li />").text(error));
                MCPError(errLst);
            };
        }

    }

}

function GetBase64DesdeTexto(pControlHtml, pObjetoFile) {
    var reader2 = new FileReader();
    var fsize = 0;
    var xExt = '';
    var xNom = '';
    var xF = false;
    var errLst = $('<ol></ol>');
    var xControlHtml = '#' + pControlHtml;

    pObjetoFile.Size = 0;
    pObjetoFile.Nombre = '';
    pObjetoFile.Data = '';

    if ($(xControlHtml)[0].files.length > 0) {
        fsize = $(xControlHtml)[0].files[0].size;
        xExt = ExtensionDelArchivo(pControlHtml);
        xNom = NombreDelArchivo(pControlHtml);

        if (!CheckEsTexto(xExt)) {
            errLst.append($("<li />").text('Tipo de Archivo digital ' + xExt.toUpperCase() + ', no válido.'));
            xF = true;
        }

        if (fsize > 157286400) {
            errLst.append($("<li />").text("El tamaño del Archivo digital debe ser menor a 150 MB."));
            xF = true;
        }

        if (xF) {
            pObjetoFile.Size = 0;
            pObjetoFile.Nombre = '';
            pObjetoFile.Data = '';
            $(xControlHtml).val('');
            MCPError(errLst);
        }
        else {
            pObjetoFile.Size = fsize;
            pObjetoFile.Nombre = xNom;

            reader2.readAsDataURL($(xControlHtml)[0].files[0]);
            reader2.onloadend = function () {

                pObjetoFile.Data = reader2.result;
            };
            reader2.onerror = function (error) {

                pObjetoFile.Size = 0;
                pObjetoFile.Nombre = '';
                pObjetoFile.Data = '';
                $(xControlHtml).val('');
                errLst.append($("<li />").text(error));
                MCPError(errLst);
            };
        }

    }

}

function GetBase64DesdePDF(pControlHtml, pObjetoFile) {
    var reader2 = new FileReader();
    var fsize = 0;
    var xExt = '';
    var xNom = '';
    var xF = false;
    var errLst = $('<ol></ol>');
    var xControlHtml = '#' + pControlHtml;

    pObjetoFile.Size = 0;
    pObjetoFile.Nombre = '';
    pObjetoFile.Data = '';

    if ($(xControlHtml)[0].files.length > 0) {
        fsize = $(xControlHtml)[0].files[0].size;
        xExt = ExtensionDelArchivo(pControlHtml);
        xNom = NombreDelArchivo(pControlHtml);

        if (!CheckEsPDF(xExt)) {
            errLst.append($("<li />").text('Tipo de Archivo digital ' + xExt.toUpperCase() + ', no válido.'));
            xF = true;
        }

        if (fsize > 8192000) {
            errLst.append($("<li />").text("El tamaño del Archivo digital debe ser menor a 8 Mb."));
            xF = true;
        }

        if (xF) {
            pObjetoFile.Size = 0;
            pObjetoFile.Nombre = '';
            pObjetoFile.Data = '';
            $(xControlHtml).val('');
            MCPError(errLst);
        }
        else {
            pObjetoFile.Size = fsize;
            pObjetoFile.Nombre = xNom;

            reader2.readAsDataURL($(xControlHtml)[0].files[0]);
            reader2.onloadend = function () {

                pObjetoFile.Data = reader2.result;
            };
            reader2.onerror = function (error) {

                pObjetoFile.Size = 0;
                pObjetoFile.Nombre = '';
                pObjetoFile.Data = '';
                $(xControlHtml).val('');
                errLst.append($("<li />").text(error));
                MCPError(errLst);
            };
        }

    }

}

function GetBase64DesdeCERT(pControlHtml, pObjetoFile) {
    var reader2 = new FileReader();
    var fsize = 0;
    var xExt = '';
    var xNom = '';
    var xF = false;
    var errLst = $('<ol></ol>');
    var xControlHtml = '#' + pControlHtml;

    pObjetoFile.Size = 0;
    pObjetoFile.Nombre = '';
    pObjetoFile.Data = '';

    if ($(xControlHtml)[0].files.length > 0) {
        fsize = $(xControlHtml)[0].files[0].size;
        xExt = ExtensionDelArchivo(pControlHtml);
        xNom = NombreDelArchivo(pControlHtml);

        if (!CheckEsCERT(xExt)) {
            errLst.append($("<li />").text('Tipo de Archivo digital ' + xExt.toUpperCase() + ', no válido.'));
            xF = true;
        }

        if (fsize > 8192000) {
            errLst.append($("<li />").text("El tamaño del Archivo digital debe ser menor a 8 Mb."));
            xF = true;
        }

        if (xF) {
            pObjetoFile.Size = 0;
            pObjetoFile.Nombre = '';
            pObjetoFile.Data = '';
            $(xControlHtml).val('');
            MCPError(errLst);
        }
        else {
            pObjetoFile.Size = fsize;
            pObjetoFile.Nombre = xNom;

            reader2.readAsDataURL($(xControlHtml)[0].files[0]);
            reader2.onloadend = function () {

                pObjetoFile.Data = reader2.result;
            };
            reader2.onerror = function (error) {

                pObjetoFile.Size = 0;
                pObjetoFile.Nombre = '';
                pObjetoFile.Data = '';
                $(xControlHtml).val('');
                errLst.append($("<li />").text(error));
                MCPError(errLst);
            };
        }

    }

}


function GetBase64DesdeImagen(pControlHtml, pObjetoFile) {
    var reader2 = new FileReader();
    var fsize = 0;
    var xExt = '';
    var xNom = '';
    var xF = false;
    var errLst = $('<ol></ol>');
    var xControlHtml = '#' + pControlHtml;

    pObjetoFile.Size = 0;
    pObjetoFile.Nombre = '';
    pObjetoFile.Data = '';

    if ($(xControlHtml)[0].files.length > 0) {
        fsize = $(xControlHtml)[0].files[0].size;
        xExt = ExtensionDelArchivo(pControlHtml);
        xNom = NombreDelArchivo(pControlHtml);

        if (!CheckEsImagen(xExt)) {
            errLst.append($("<li />").text('Tipo de Archivo digital ' + xExt.toUpperCase() + ', no válido.'));
            xF = true;
        }

        if (fsize > 8192000) {
            errLst.append($("<li />").text("El tamaño del Archivo digital debe ser menor a 8 Mb."));
            xF = true;
        }

        if (xF) {
            pObjetoFile.Size = 0;
            pObjetoFile.Nombre = '';
            pObjetoFile.Data = '';
            $(xControlHtml).val('');
            MCPError(errLst);
        }
        else {
            pObjetoFile.Size = fsize;
            pObjetoFile.Nombre = xNom;

            reader2.readAsDataURL($(xControlHtml)[0].files[0]);
            reader2.onloadend = function () {

                pObjetoFile.Data = reader2.result;
            };
            reader2.onerror = function (error) {

                pObjetoFile.Size = 0;
                pObjetoFile.Nombre = '';
                pObjetoFile.Data = '';
                $(xControlHtml).val('');
                errLst.append($("<li />").text(error));
                MCPError(errLst);
            };
        }

    }

}