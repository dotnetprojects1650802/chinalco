﻿
function CerrarDialogo(pPopUp) {
    var xPopup = '#' + pPopUp;
    $(xPopup).modal('hide');
}

function CheckEsEmail(pEmail) {
    var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

    if (emailRegex.test(pEmail)) {
        return true;
    } else {
        return false;
    }
}

function FormatFecha(pFecha) {
    var xRes = '';

    for (x = 0; x < pFecha.length; x++) {
        if (pFecha[x] === '/') xRes += '-';
        else xRes += pFecha[x];
    }

    return (xRes);
}

function FechaActual() {
    var date = new Date();
    var xAnio = date.getFullYear();
    var xMes = date.getMonth() + 1;
    var xDia = date.getDate();
    var xFi = '';
    var xMm = '';
    var xDd = '';

    if (xMes < 10) xMm = '0' + xMes.toString();
    else xMm = xMes.toString();

    if (xDia < 10) xDd = '0' + xDia.toString();
    else xDd = xDia.toString();

    xFi = xDd + '/' + xMm + '/' + xAnio.toString();

    return (xFi);
}

function MostrarDialogo(pPopUp) {
    var xPopup = '#' + pPopUp;

    $(xPopup).modal({ backdrop: 'static', show: true });
}

function MostrarSpinner(pDiv) {
    var opts = {
        lines: 13, // The number of lines to draw
        length: 40, // The length of each line
        width: 3, // The line thickness
        radius: 40, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#000', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: true, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: '50%', // Top position relative to parent
        left: '50%' // Left position relative to parent
    };

    var target = document.getElementById(pDiv);
    spinner = new Spinner(opts).spin(target);
}

function MCPError(pMensaje) {

    BootstrapDialog.show({ title: 'Error - validación de datos', message: pMensaje, type: BootstrapDialog.TYPE_DANGER, draggable: true, backdrop: 'static' });
}

function MCPError2(pJsonData) {
    var xData = JSON.parse(pJsonData);
    var xC = xData.length;
    var errLst = $('<ol></ol>');

    if (xC > 0) {
        for (x = 0; x < xData.length; x++) {
            errLst.append($("<li />").text(xData[x].Descripcion));
        }

        BootstrapDialog.show({
            title: 'Error - Validaci&oacute;n de datos',
            message: errLst,
            type: BootstrapDialog.TYPE_DANGER,
            draggable: true,
            closable: true,
            closeByBackdrop: false,
            closeByKeyboard: false,
            backdrop: 'static'
        });        
    }
}

function MCPtrim(pTexto) {
    return pTexto.replace(/^\s+|\s+$/g, "");
}

function MCPInformarAccionCompletada(pTitulo, pMensaje) {

    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_INFO,
        title: pTitulo,
        message: pMensaje,
        draggable: true,
        closable: true,
        closeByBackdrop: false,
        closeByKeyboard: false,
        buttons: [{
            label: 'Aceptar',
            action: function (dialogRef) {
                dialogRef.close();
            }
        }]
    });
}

function NavegarA(pUrl) {
    var _xPosCar = pUrl.indexOf('?', 0);
    var _xxUrl;

    if (_xPosCar == -1) _xxUrl = pUrl + '?RND=' + Date.now();
    else _xxUrl = pUrl + '&RND=' + Date.now();

    window.location.replace(_xxUrl);
}

function PrimerDiaFechaActual() {
    var date = new Date();
    var xAnio = date.getFullYear();
    var xMes = date.getMonth() + 1;
    var xFi = '';

    if (xMes < 10) {
        xFi = '01/0' + xMes.toString() + '/' + xAnio.toString();
    }
    else {
        xFi = '01/' + xMes.toString() + '/' + xAnio.toString();
    }

    return (xFi);
}

function OcultarSpinner() {
    if (spinner !== null) spinner.stop();

    spinner = null;
}

function SoloNros(xCode) {
    if ((xCode < 48) || (xCode > 57)) event.returnValue = false;
}

function SoloDecimal(event, obj) {
    // Get the ASCII value of the key that the user entered compatible with browsers
    var key = (event.which) ? event.which : event.keyCode

    if (obj.value.indexOf(".") == -1 && key == 190) {
        return true;
    }
    else if (obj.value.indexOf(".") == -1 && key == 110) {
        return true;
    }
    if ((event.shiftKey || key == 16 || key == 93) && key != 9) {
        // If it was not, then dispose the key and continue with entry
        event.returnValue = null;
        try {
            event.preventDefault();
        } catch (err) {
            //this is IE
        }
        return false;

    }
    var validVal = "0123456789";

    //allow arrow keys , delete,BACKSPACE,tab
    if (key == 9 || key == 37 || key == 39 || key == 35 || key == 36 || key == 46 || key == 8 || (key >= 96 && key <= 105)) {
        return true;
    }

    if (validVal.indexOf(String.fromCharCode(key)) == -1) {

        // If it was not, then dispose the key and continue with entry
        event.returnValue = null;
        try {
            event.preventDefault();
        } catch (err) {
            //this is IE
        }
        return false;
    }
    return true;
}


function VerificarFecha(xFecha) {
    var xD = "", xM = "", xA = "";
    var yMes;
    var xFecha;
    var yDia;

    if (xFecha.length > 9) {
        xD = xFecha.substr(0, 2);
        xM = xFecha.substr(3, 2);
        xA = xFecha.substr(6, 4);
    }
    else return false;

    if (xD == "") return false;

    if (xD.length == 1) return false;

    if (xD > 31) return false;

    if (xM == "") return false;

    if (xM.length == 1) return false;

    if (xM > 12) return false;

    if (xA == "") return false;

    if (xA.length < 4) return false;

    yMes = xM - 1;
    xFecha = new Date(xA, yMes, xD, 00, 00, 00, 000);
    yDia = xFecha.getDate();

    if (yDia == xD)
        return true;
    else
        return false;
}

function HabilitarControl(pControl) {
    var xControl = '#' + pControl;

    $(xControl).removeAttr("disabled");
}

function BloquearControl(pControl) {
    var xControl = '#' + pControl;

    $(xControl).prop('disabled', 'disabled');
}

function CheckEsNumero(pDato) {
    if (isNaN(pDato)) return (0);
    else return (parseInt(pDato));
}

function AbrirDigital(pUrl)
{
   
    if (navigator.platform == 'iPad' || navigator.platform == 'iPhone' || navigator.platform == 'iPod') {
        window.open(pUrl);
    }
    else document.location.href = pUrl;

};

function AbrirArchivoEnVentanaNueva(pUrl) {

    window.open(pUrl, '_blank', "status=no;toolbar=no;menubar=no;titlebar=no;location=no");
};

function MyTrim(x) {
    var xResult = $.trim(x);

    return (xResult);
}

function ExtensionDelArchivo(pFile) {
    var xFile = '#' + pFile;
    var xRes = false;
    var xExt = '';
    xFile = $(xFile).val();

    fileName = xFile.split('\\');
    fileName = fileName[fileName.length - 1];
    extention = fileName.split('.');

    xExt = extention[extention.length - 1];

    return (xExt)
}

function NombreDelArchivo(pInputFile) {
    var xFile = '#' + pInputFile;
    var xRes = false;
    var xExt = '';

    xFile = $(xFile).val();

    fileName = xFile.split('\\');
    xExt = fileName[fileName.length - 1];

    return (xExt)
}

function CheckInpeccionArchivo(pExt) {
    var xF = false;

    switch (pExt.toUpperCase()) {
        case 'PDF':
            xF = true;
            break;
        case 'JPG':
            xF = true;
            break;
        case 'PNG':
            xF = true;
            break;
        case 'GIF':
            xF = true;
            break;
        case 'BMP':
            xF = true;
            break;
        case 'JPEG':
            xF = true;
            break;
    }

    return (xF);
}

function NoEsFecha(xDia, xMes, xAno) {
    var yMes = xMes - 1;
    var xFecha = new Date(xAno, yMes, xDia, 00, 00, 00, 000);
    var oMes = xFecha.getMonth() + 1;
    var oDia = xFecha.getDate();
    var oAno = xFecha.getFullYear();

    if (xDia == "") return true;

    if (xDia.length == 1) return true;

    if (xDia > 31) return true;

    if (xMes == "") return true;

    if (xMes.length == 1) return true;

    if (xMes > 12) return true;

    if (xAno == "") return true;

    if (xAno.length < 4) return true;

    if (oDia == xDia)
        return false;
    else
        return true;
}

function VerificarNoEsFecha(xFecha) {
    var xD = "", xM = "", xA = "";

    if (xFecha.length > 9) {
        xD = xFecha.substr(0, 2);
        xM = xFecha.substr(3, 2);
        xA = xFecha.substr(6, 4);
    }

    return (NoEsFecha(xD, xM, xA));
}

function SoloLetras(xCode) {   
    var tecla = String.fromCharCode(xCode).toLowerCase();
    var letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    var especiales = "8-37-39-46";

    tecla_especial = false;

    for (var i in especiales) {
        if (xCode == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        event.returnValue = false;
    }
}

function MCPMensajeWarning(pJsonData, pTitulo) {
    var xData = JSON.parse(pJsonData);
    var xC = xData.length;
    var errLst = $('<ol></ol>');

    if (xC > 0) {
        for (x = 0; x < xData.length; x++) {
            errLst.append($("<li />").text(xData[x].Descripcion));
        }

        BootstrapDialog.show({
            title: pTitulo,
            message: errLst,
            type: BootstrapDialog.TYPE_WARNING,
            draggable: true,
            closable: true,
            closeByBackdrop: false,
            closeByKeyboard: false,
            backdrop: 'static'
        });
    }
}


