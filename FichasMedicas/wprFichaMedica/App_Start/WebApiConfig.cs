﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Chinalco.RRHH.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{action}/{pParametro}", (object)new
            {
                pParametro = RouteParameter.Optional
            });
        }
    }
}
