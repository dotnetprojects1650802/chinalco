﻿
using Chinalco.Shared;

namespace Chinalco.RRHH.Salud.TO
{
  public class TOSaludCatalogoItem : TOBaseI
  {
        public int KeyCatalogo { get; set; }
        public string FechaRegistro { get; set; }
        public string RegistradoPor { get; set; }

        public TOSaludCatalogoItem()
        {
            this.Key = 0;
            this.Nombre = string.Empty;
            this.FechaRegistro = string.Empty;
            this.RegistradoPor = string.Empty;           
        }
  }
}
