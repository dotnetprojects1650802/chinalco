﻿
using Chinalco.Shared;

namespace Chinalco.RRHH.Salud.TO
{
  public class TOSaludResultado : TOBaseI
  {
        public TOBaseI Empresa { get; set; }

        public string Gerencia { get; set; }
        public string Fecha { get; set; }
        public string Usuario { get; set; }

        public TOSaludResultado()
        {
            this.Key = 0;
            this.Nombre = string.Empty;
            this.Gerencia = string.Empty;
            this.Fecha = string.Empty;
            this.Usuario = string.Empty;

            Empresa = new TOBaseI();
        }
  }
}
