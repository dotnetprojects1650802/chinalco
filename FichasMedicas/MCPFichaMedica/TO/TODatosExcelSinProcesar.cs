﻿
using System;

namespace Chinalco.RRHH.Salud.TO
{
    [Serializable]
    public class TODatosExcelSinProcesar
    {
        public int Index { get; set; }
        public string Nombre { get; set; }
        public string TipoDOI { get; set; }
        public string NumeroDOI { get; set; }
        public string Empresa { get; set; }
        public TODatosExcelSinProcesar()
        {
            Index = 0;
            Nombre = string.Empty;
            TipoDOI = string.Empty;
            NumeroDOI = string.Empty;
            Empresa = string.Empty;
        }
    }
}
