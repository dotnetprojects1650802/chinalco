﻿
using Chinalco.Shared;

namespace Chinalco.RRHH.Salud.TO
{
    public class TOPersonalContratista : TOBaseI
    {
        public string NumeroDocumento { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string Crea { get; set; }
        public string Modifica { get; set; }
        public string Creacion { get; set; }
        public string Modificacion { get; set; }
        public string EstadoOperacion { get; set; }
        public TOBaseI Contratista { get; set; }
        public TOBaseI Empresa { get; set; }
        public TOBaseI TipoDocumento { get; set; }
        public TOPersonalContratista()
        {
            NumeroDocumento = string.Empty;
            ApellidoPaterno = string.Empty;
            ApellidoMaterno = string.Empty;
            PrimerNombre = string.Empty;
            SegundoNombre = string.Empty;
            Crea = string.Empty;
            Modifica = string.Empty;
            Creacion = string.Empty;
            Modificacion = string.Empty;
            EstadoOperacion = string.Empty;
            Contratista = new TOBaseI();
            Empresa = new TOBaseI();
            TipoDocumento = new TOBaseI();
        }
    }
}
