﻿
namespace Chinalco.RRHH.Salud.TO
{
    public class TODatosExcelPersonalContratista
    {
        public string Tipo { get; set; }
        public string Nombre { get; set; }
        public string Sexo { get; set; }
        public string FechaNacimiento { get; set; }
        public string Fotocheck { get; set; }
        public string FechaInicioProceso { get; set; }
        public string TipoDOI { get; set; }
        public string NumeroDOI { get; set; }
        public string Empresa { get; set; }
        public string Estado { get; set; }
        public string CodigoRFI { get; set; }
        public string Puesto { get; set; }
        public string Area { get; set; }
        public string CodigoContrato { get; set; }
        public string TipoContrato { get; set; }
        public string InicioContrato { get; set; }
        public string FinContrato { get; set; }
        public string AdministradorContrato { get; set; }
        public string DescripcionContrato { get; set; }
        public string Procedencia { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string Direccion { get; set; }
        public string AdministradorContratos { get; set; }
        public string TipoManoDeObra { get; set; }
        public string LugarTrabajo { get; set; }
        public string CorreoElectronico { get; set; }
        public string Telefono { get; set; }
        public string CodigoUbigeo { get; set; }
        public string EstadoCivil { get; set; }
        public string NumeroHijos { get; set; }
        public string TelefonoContacto { get; set; }
        public string PersonaContacto { get; set; }
        public string NivelEstudios { get; set; }
        public string Profesion { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public TODatosExcelPersonalContratista()
        {
            Tipo = string.Empty;
            Nombre = string.Empty;
            Sexo = string.Empty;
            FechaNacimiento = string.Empty;
            Fotocheck = string.Empty;
            FechaInicioProceso = string.Empty;
            TipoDOI = string.Empty;
            NumeroDOI = string.Empty;
            Empresa = string.Empty;
            Estado = string.Empty;
            CodigoRFI = string.Empty;
            Puesto = string.Empty;
            Area = string.Empty;
            CodigoContrato = string.Empty;
            TipoContrato = string.Empty;
            InicioContrato = string.Empty;
            FinContrato = string.Empty;
            AdministradorContrato = string.Empty;
            DescripcionContrato = string.Empty;
            Procedencia = string.Empty;
            Departamento = string.Empty;
            Provincia = string.Empty;
            Distrito = string.Empty;
            Direccion = string.Empty;
            AdministradorContratos = string.Empty;
            TipoManoDeObra = string.Empty;
            LugarTrabajo = string.Empty;
            CorreoElectronico = string.Empty;
            Telefono = string.Empty;
            CodigoUbigeo = string.Empty;
            EstadoCivil = string.Empty;
            NumeroHijos = string.Empty;
            TelefonoContacto = string.Empty;
            PersonaContacto = string.Empty;
            NivelEstudios = string.Empty;
            Profesion = string.Empty;
            ApellidoPaterno = string.Empty;
            ApellidoMaterno = string.Empty;
            PrimerNombre = string.Empty;
            SegundoNombre = string.Empty;
        }
    }
}
