﻿
using Chinalco.Shared;

namespace Chinalco.RRHH.Salud.TO
{
  public class TOSaludEvaluacion 
  {
        public int Key { get; set; }
        public int Anio { get; set; }
        public int KeyResultado { get; set; }
        public string FileCompleto { get; set; }
        public string TienePlaca { get; set; }
        public string TieneRestriccion { get; set; }
        public string Observacion { get; set; }
        public string Fecha { get; set; }
        public string Resonancia { get; set; }
        public string TrabajoAltura { get; set; }
        public string Psicosensometrico { get; set; }

        public TOSaludColaborador Colaborador { get; set; }       
        public TOBaseI TipoExamen { get; set; }
        public TOBaseI TipoResultado { get; set; }
        public TOBaseI Medico { get; set; }
        public TOBaseI Clinica { get; set; }
        public TOBaseI EmpresaEvaluadora { get; set; }

        public TOBinarioS Archivo { get; set; }
        public TOBinarioS PlacaF { get; set; }
        public TOBinarioS PlacaL { get; set; }

        public TOSaludEvaluacion()
        {
            this.Key = 0;
            this.KeyResultado = 0;
            this.Anio = 0;
            
            this.Fecha = string.Empty;
            this.Observacion = string.Empty;
            this.FileCompleto = "N";
            this.TienePlaca = "N";
            this.TieneRestriccion= "N";
            this.Resonancia = "N";
            this.TrabajoAltura = "N";
            this.Psicosensometrico = "N";

            Colaborador = new TOSaludColaborador();
        
            TipoResultado = new TOBaseI();
            TipoExamen = new TOBaseI();
            Medico = new TOBaseI();
            Clinica = new TOBaseI();
            EmpresaEvaluadora = new TOBaseI();

            Archivo = new TOBinarioS();
            PlacaF = new TOBinarioS();
            PlacaL = new TOBinarioS();
        }
  }
}
