﻿
using Chinalco.Shared;

namespace Chinalco.RRHH.Salud.TO
{
    public class TOImportarPersonalContratista
    {
        public TOBinarioS ExcelPersonalContratista { get; set; }
        public TOImportarPersonalContratista()
        {
            ExcelPersonalContratista = new TOBinarioS();
        }
    }
}
