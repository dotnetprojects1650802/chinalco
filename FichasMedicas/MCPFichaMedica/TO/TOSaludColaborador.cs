﻿
using Chinalco.Shared;

namespace Chinalco.RRHH.Salud.TO
{
  public class TOSaludColaborador : TOBaseI
  {
        public int KeyResultado { get; set; }
        public TOBaseI Empresa { get; set; }
     
        public TOSaludColaborador()
        {
            this.Key = 0;
            this.Nombre = string.Empty;
            this.KeyResultado = 0;

            Empresa = new TOBaseI();
        }
  }
}
