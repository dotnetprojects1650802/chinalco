﻿
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

using Chinalco.RRHH.Salud.TO;
using Chinalco.Shared;

namespace Chinalco.RRHH.Salud.DB
{
  internal class DBSaludResultado
  {
    private string _xError = string.Empty;

        public string Error()
        {
            return (this._xError);
        }

        public Collection<TOSaludResultado> ListarResultados(string pTextoBusqueda)
        {
            DBConexionSalud dbConexion = new DBConexionSalud();
            SqlDataReader sqlDataReader = null;
            SqlCommand sqlCommand = new SqlCommand();
            Collection<TOSaludResultado> oLista = new Collection<TOSaludResultado>();

            this._xError = "";

            try
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "que_ResultadoMedico_Consultar";

                sqlCommand.Parameters.AddWithValue("@vcTexto", pTextoBusqueda);

                sqlCommand.Connection = dbConexion.AbrirModoLectura();
                sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    TOSaludResultado oTo = new TOSaludResultado();
                    oTo.Key = sqlDataReader.GetInt32(0);
                    oTo.Nombre = sqlDataReader.GetString(1);
                    oTo.Empresa.Key = sqlDataReader.GetInt32(2);
                    oTo.Empresa.Nombre= sqlDataReader.GetString(3);
                    oTo.Gerencia= sqlDataReader.GetString(4);
                    oTo.Fecha = sqlDataReader.GetString(5);
                    oTo.Usuario = sqlDataReader.GetString(6);

                    oLista.Add(oTo);
                }
            }
            catch (Exception ex)
            {
                this._xError = ex.Message;
                Comun.EventosGrabar("MCPRRHHSalud", "DBSaludResultado.ListarResultados", ex);
            }
            finally
            {
                sqlDataReader?.Dispose();
                sqlCommand.Dispose();
                dbConexion.Dispose();
            }
      
            return (oLista);
        }

        public Collection<TOSaludEvaluacion> ListarEvaluaciones(int pResultadoMedico)
        {
            DBConexionSalud dbConexion = new DBConexionSalud();
            SqlDataReader sqlDataReader = null;
            SqlCommand sqlCommand = new SqlCommand();
            Collection<TOSaludEvaluacion> oLista = new Collection<TOSaludEvaluacion>();

            this._xError = "";

            try
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "que_ResultadoMedicoDetalle_Consultar";

                sqlCommand.Parameters.AddWithValue("@inResultado", pResultadoMedico);

                sqlCommand.Connection = dbConexion.AbrirModoLectura();
                sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    TOSaludEvaluacion oTo = new TOSaludEvaluacion();
                    oTo.Key = sqlDataReader.GetInt32(0);
                    oTo.KeyResultado = sqlDataReader.GetInt32(1);
                    oTo.Fecha = sqlDataReader.GetString(2);
                    oTo.Anio = sqlDataReader.GetInt32(3);
                    oTo.TipoExamen.Key = sqlDataReader.GetInt32(4);
                    oTo.TipoExamen.Nombre = sqlDataReader.GetString(5);

                    oTo.TipoResultado.Key = sqlDataReader.GetInt32(6);
                    oTo.TipoResultado.Nombre = sqlDataReader.GetString(7);
                    oTo.EmpresaEvaluadora.Key = sqlDataReader.GetInt32(8);
                    oTo.EmpresaEvaluadora.Nombre = sqlDataReader.GetString(9);
                    oTo.Clinica.Key = sqlDataReader.GetInt32(10);
                    oTo.Clinica.Nombre = sqlDataReader.GetString(11);
                    oTo.TienePlaca = sqlDataReader.GetString(12);

                    oLista.Add(oTo);
                }
            }
            catch (Exception ex)
            {
                this._xError = ex.Message;
                Comun.EventosGrabar("MCPRRHHSalud", "DBSaludResultado.ListarEvaluaciones", ex);
            }
            finally
            {
                sqlDataReader?.Dispose();
                sqlCommand.Dispose();
                dbConexion.Dispose();
            }

            return (oLista);
        }
        public Collection<TOSaludEvaluacion> ListarEvaluacionesPorPersona(int pPersona)
        {
            DBConexionSalud dbConexion = new DBConexionSalud();
            SqlDataReader sqlDataReader = null;
            SqlCommand sqlCommand = new SqlCommand();
            Collection<TOSaludEvaluacion> oLista = new Collection<TOSaludEvaluacion>();

            this._xError = "";

            try
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "que_ResultadoMedicoDetalle_ConsultarPorPersona";

                sqlCommand.Parameters.AddWithValue("@inPersonal", pPersona);

                sqlCommand.Connection = dbConexion.AbrirModoLectura();
                sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    TOSaludEvaluacion oTo = new TOSaludEvaluacion();
                    oTo.Key = sqlDataReader.GetInt32(0);
                    oTo.KeyResultado = sqlDataReader.GetInt32(1);
                    oTo.Fecha = sqlDataReader.GetString(2);
                    oTo.Anio = sqlDataReader.GetInt32(3);
                    oTo.TipoExamen.Key = sqlDataReader.GetInt32(4);
                    oTo.TipoExamen.Nombre = sqlDataReader.GetString(5);

                    oTo.TipoResultado.Key = sqlDataReader.GetInt32(6);
                    oTo.TipoResultado.Nombre = sqlDataReader.GetString(7);
                    oTo.EmpresaEvaluadora.Key = sqlDataReader.GetInt32(8);
                    oTo.EmpresaEvaluadora.Nombre = sqlDataReader.GetString(9);
                    oTo.Clinica.Key = sqlDataReader.GetInt32(10);
                    oTo.Clinica.Nombre = sqlDataReader.GetString(11);
                    oTo.FileCompleto= sqlDataReader.GetString(12);
                    oTo.TienePlaca= sqlDataReader.GetString(13);
                    oTo.TieneRestriccion= sqlDataReader.GetString(14);
                    oTo.Observacion= sqlDataReader.GetString(15);
                    oTo.Medico.Key= sqlDataReader.GetInt32(16);
                    oTo.Medico.Nombre= sqlDataReader.GetString(17);
                    oTo.Psicosensometrico= sqlDataReader.GetString(18);
                    oTo.TrabajoAltura= sqlDataReader.GetString(19);
                    oTo.Resonancia= sqlDataReader.GetString(20);

                    oTo.PlacaF.Size = sqlDataReader.GetInt32(21);
                    oTo.PlacaL.Size = sqlDataReader.GetInt32(22);

                    oLista.Add(oTo);
                }
            }
            catch (Exception ex)
            {
                this._xError = ex.Message;
                Comun.EventosGrabar("MCPRRHHSalud", "DBSaludResultado.ListarEvaluacionesPorPersona", ex);
            }
            finally
            {
                sqlDataReader?.Dispose();
                sqlCommand.Dispose();
                dbConexion.Dispose();
            }

            return (oLista);
        }

        public TOBinario BuscarArchivo(int pEvaluacion, int pTipo)
        {
            DBConexionSalud dbConexion = new DBConexionSalud();
            SqlDataReader sqlDataReader = (SqlDataReader)null;
            SqlCommand sqlCommand = new SqlCommand();
            SqlBinary sqlBinary1 = new SqlBinary();
            TOBinario toBinario = new TOBinario();

            try
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "que_ResultadoMedicoDetalle_BuscarArchivo";

                sqlCommand.Parameters.AddWithValue("@inEvaluacion", pEvaluacion);
                sqlCommand.Parameters.AddWithValue("@inTipo", pTipo);

                sqlCommand.Connection = dbConexion.AbrirModoLectura();
                sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    sqlBinary1 = sqlDataReader.GetSqlBinary(2);

                    toBinario = new TOBinario()
                    {                     
                        Nombre = sqlDataReader[0].ToString(),
                        TipoMIME = sqlDataReader[1].ToString(),
                        Tamanio = sqlBinary1.Length
                    };
                    toBinario.Datos = new byte[toBinario.Tamanio];
                    sqlDataReader.GetBytes(2, 0L, toBinario.Datos, 0, toBinario.Tamanio);
                }
            }
            catch (Exception ex)
            {
                this._xError = ex.Message;
                Comun.EventosGrabar("MCPRRHHSalud", "DBSaludResultado.BuscarArchivo", ex);
            }
            finally
            {
                sqlDataReader?.Dispose();
                sqlCommand.Dispose();
                dbConexion.Dispose();
            }

            return toBinario;
        }

        public bool RegistrarResultado(TOSaludEvaluacion pEvaluacion, TOBinario pArchivo, TOBinario pPlacaF, TOBinario pPlacaL, DateTime pFecha, string pCodUsuario)
        {
            DBConexionSalud dbConexion = new DBConexionSalud();
            SqlCommand oSqlCmd = new SqlCommand();
            SqlConnection oSqlCon = new SqlConnection();
            SqlTransaction oSqlTrans = null;
            bool xRes = false;
            int xKey = 0;

            this._xError = "";

            try
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.CommandText = "ins_ResultadoMedico_Registrar";
                oSqlCmd.Parameters.AddWithValue("@inColaborador", pEvaluacion.Colaborador.Key);
                oSqlCmd.Parameters.AddWithValue("@inEmpresa", pEvaluacion.Colaborador.Empresa.Key);
                oSqlCmd.Parameters.AddWithValue("@vcUsuario", pCodUsuario);

                oSqlCmd.Parameters.Add("@inKey", SqlDbType.Int);
                oSqlCmd.Parameters["@inKey"].Direction = ParameterDirection.Output;

                oSqlCon = dbConexion.AbrirModoEscritura();
                oSqlTrans = oSqlCon.BeginTransaction();
                oSqlCmd.Connection = oSqlTrans.Connection;
                oSqlCmd.Transaction = oSqlTrans;
                oSqlCmd.ExecuteNonQuery();

                xKey = (int)oSqlCmd.Parameters["@inKey"].Value;

                this.RegistrarEvaluacion(xKey, pEvaluacion, pArchivo, pPlacaF, pPlacaL, pFecha, oSqlTrans, pCodUsuario);

                oSqlTrans.Commit();
                xRes = true;
               
            }
            catch (Exception ex)
            {
                xRes = false;
                this._xError = ex.Message;

                oSqlTrans?.Rollback();
                Comun.EventosGrabar("MCPRRHHSalud", "DBSaludResultado.RegistrarResultado", ex);
            }
            finally
            {
                oSqlTrans?.Dispose();
                oSqlCmd.Dispose();
                oSqlCon.Dispose();
                dbConexion.Dispose();
            }

            return (xRes);
        }

        public bool BorrarEvaluacion(int pEvaluacion, string pCodUsuario)
        {
            DBConexionSalud dbConexion = new DBConexionSalud();
            SqlCommand oSqlCmd = new SqlCommand();          
            bool xRes = false;
           
            this._xError = "";

            try
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.CommandText = "del_ResultadoMedicoDetalle_Borrar";

                oSqlCmd.Parameters.AddWithValue("@inEvaluacion", pEvaluacion);                
                oSqlCmd.Parameters.AddWithValue("@vcUsuario", pCodUsuario);
                            
                oSqlCmd.Connection = dbConexion.AbrirModoEscritura();
              
                oSqlCmd.ExecuteNonQuery();                              
              
                xRes = true;

            }
            catch (Exception ex)
            {
                xRes = false;
                this._xError = ex.Message;
               
                Comun.EventosGrabar("MCPRRHHSalud", "DBSaludResultado.BorrarEvaluacion", ex);
            }
            finally
            {                
                oSqlCmd.Dispose();               
                dbConexion.Dispose();
            }

            return (xRes);
        }
        private bool RegistrarEvaluacion(int pKeyResultado, TOSaludEvaluacion pEvaluacion, TOBinario pArchivo, TOBinario pPlacaF, TOBinario pPlacaL, DateTime pFecha, SqlTransaction pSqlTrans, string pUsuario)
        {
            SqlCommand oSqlCmd = new SqlCommand();
            bool xRes = false;

            this._xError = "";

            try
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.CommandText = "ins_ResultadoMedicoDetalle_Registrar";

                oSqlCmd.Parameters.AddWithValue("@inResultado", pKeyResultado);
                oSqlCmd.Parameters.AddWithValue("@inEvaluacion", pEvaluacion.Key);
                oSqlCmd.Parameters.AddWithValue("@inClinica", pEvaluacion.Clinica.Key);
                oSqlCmd.Parameters.AddWithValue("@inMedico", pEvaluacion.Medico.Key);
                oSqlCmd.Parameters.AddWithValue("@inTipoExamen", pEvaluacion.TipoExamen.Key);
                oSqlCmd.Parameters.AddWithValue("@inTipoResultado", pEvaluacion.TipoResultado.Key);
                oSqlCmd.Parameters.AddWithValue("@inEmpEvaluadora", pEvaluacion.EmpresaEvaluadora.Key);
                oSqlCmd.Parameters.AddWithValue("@inAnio", pEvaluacion.Anio);
                oSqlCmd.Parameters.AddWithValue("@chFecha", pFecha.ToString("yyyyMMdd"));
                oSqlCmd.Parameters.AddWithValue("@chFileCompleto", pEvaluacion.FileCompleto);
                oSqlCmd.Parameters.AddWithValue("@chTienePlaca", pEvaluacion.TienePlaca);
                oSqlCmd.Parameters.AddWithValue("@chTieneRestri", pEvaluacion.TieneRestriccion);

                oSqlCmd.Parameters.AddWithValue("@chPsicosenso", pEvaluacion.Psicosensometrico);
                oSqlCmd.Parameters.AddWithValue("@chAltura", pEvaluacion.TrabajoAltura);
                oSqlCmd.Parameters.AddWithValue("@chResonancia", pEvaluacion.Resonancia);

                oSqlCmd.Parameters.AddWithValue("@vcObservacion", pEvaluacion.Observacion);                
                oSqlCmd.Parameters.AddWithValue("@vcArchivo", pArchivo.Nombre);
                oSqlCmd.Parameters.AddWithValue("@vcMime", pArchivo.TipoMIME);
                oSqlCmd.Parameters.AddWithValue("@vbData", pArchivo.Datos);
                oSqlCmd.Parameters.AddWithValue("@inTamanio", pArchivo.Tamanio);

                oSqlCmd.Parameters.AddWithValue("@vcPlacaFArchivo", pPlacaF.Nombre);
                oSqlCmd.Parameters.AddWithValue("@vcPlacaFMime", pPlacaF.TipoMIME);
                oSqlCmd.Parameters.AddWithValue("@vbPlacaFData", pPlacaF.Datos);
                oSqlCmd.Parameters.AddWithValue("@inPlacaFTamanio", pPlacaF.Tamanio);

                oSqlCmd.Parameters.AddWithValue("@vcPlacaLArchivo", pPlacaL.Nombre);
                oSqlCmd.Parameters.AddWithValue("@vcPlacaLMime", pPlacaL.TipoMIME);
                oSqlCmd.Parameters.AddWithValue("@vbPlacaLData", pPlacaL.Datos);
                oSqlCmd.Parameters.AddWithValue("@inPlacaLTamanio", pPlacaL.Tamanio);

                oSqlCmd.Parameters.AddWithValue("@vcUsuario", pUsuario);

                oSqlCmd.Connection = pSqlTrans.Connection;
                oSqlCmd.Transaction = pSqlTrans;
                oSqlCmd.ExecuteNonQuery();

                xRes=true;
            }
            catch (Exception ex)
            {
                xRes = false;
                this._xError = ex.Message;
                Comun.EventosGrabar("MCPRRHHSalud", "DBSaludResultado.RegistrarEvaluacion", ex);
                throw ex;
            }
            finally
            {
                oSqlCmd.Dispose();
            }

            return (xRes);
        }

    }
}
