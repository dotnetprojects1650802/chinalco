﻿
using System;
using System.Data;
using System.Data.SqlClient;
using Chinalco.Shared;

namespace Chinalco.RRHH.Salud.DB
{
    internal class DBImportarPersonalContratista
    {
        private string _xError = string.Empty;
        public string Error()
        {
            return (_xError);
        }

        public bool RegistrarExcelPersonalContratista(DataTable pDatos, string pCodUsuario)
        {
            DBConexionImportarPersonalContratista dbConexion = new DBConexionImportarPersonalContratista();
            SqlCommand oSqlCmd = new SqlCommand();
            bool xRes = false;

            _xError = "";

            try
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.CommandText = "ins_ExcelPersonalContratista_Registrar";
                // Agregar el parámetro de tabla estructurada
                SqlParameter parameter = oSqlCmd.Parameters.AddWithValue("@DtPersonalContratista", pDatos);
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.TypeName = "dbo.TableTypePersonalContratista";

                oSqlCmd.Parameters.AddWithValue("@vcUsuario", pCodUsuario);

                oSqlCmd.Connection = dbConexion.AbrirModoEscritura();
                oSqlCmd.ExecuteNonQuery();

                var resultado = oSqlCmd.ExecuteScalar();

                bool resultadoBooleano = (bool)(resultado);

                xRes = resultadoBooleano;
            }
            catch (Exception ex)
            {
                xRes = false;
                _xError = ex.Message;

                Comun.EventosGrabar("MCPFichaMedica", "DBImportarPersonalContratista.GrabarExcelPersonalContratista", ex);
            }
            finally
            {
                oSqlCmd.Dispose();
                dbConexion.Dispose();
            }

            return (xRes);
        }

        public bool EsValidoEmpresaRazonSocial(string pRazonSocial)
        {
            DBConexionImportarPersonalContratista dbConexion = new DBConexionImportarPersonalContratista();
            SqlCommand oSqlCmd = new SqlCommand();
            bool xRes = false;

            _xError = "";

            try
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.CommandText = "que_TerceroEmpresaRazonSocial_Consultar";
                oSqlCmd.Parameters.AddWithValue("@vcRazonSocial", pRazonSocial);

                oSqlCmd.Connection = dbConexion.AbrirModoEscritura();
                oSqlCmd.ExecuteNonQuery();

                var xResultado = oSqlCmd.ExecuteScalar();
                xRes = (bool)(xResultado);
            }
            catch (Exception ex)
            {
                xRes = false;
                _xError = ex.Message;

                Comun.EventosGrabar("MCPFichaMedica", "DBPersonalContratista.EsValidoNumeroDocumento", ex);
            }
            finally
            {
                oSqlCmd.Dispose();
                dbConexion.Dispose();
            }

            return (xRes);
        }
    }
}
