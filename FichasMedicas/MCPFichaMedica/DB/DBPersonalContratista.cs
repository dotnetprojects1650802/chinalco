﻿
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using Chinalco.RRHH.Salud.TO;
using Chinalco.Shared;

namespace Chinalco.RRHH.Salud.DB
{
    internal class DBPersonalContratista
    {
        private string _xError = string.Empty;
        public string Error()
        {
            return (_xError);
        }
        public Collection<TOPersonalContratista> ListarPersonalContratista(string pTextoBusqueda)
        {
            DBConexionPersonalContratista dbConexion = new DBConexionPersonalContratista();
            SqlDataReader sqlDataReader = null;
            SqlCommand sqlCommand = new SqlCommand();
            Collection<TOPersonalContratista> oLista = new Collection<TOPersonalContratista>();

            _xError = "";

            try
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "que_PersonalContratista_Consultar";

                sqlCommand.Parameters.AddWithValue("@vcTexto", pTextoBusqueda);

                sqlCommand.Connection = dbConexion.AbrirModoLectura();
                sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    TOPersonalContratista oTo = new TOPersonalContratista();
                    oTo.Contratista.Key = sqlDataReader.GetInt32(0);
                    oTo.Contratista.Nombre = sqlDataReader.GetString(1);
                    oTo.Empresa.Key = sqlDataReader.GetInt32(2);
                    oTo.Empresa.Nombre = sqlDataReader.GetString(3);
                    oTo.TipoDocumento.Key = sqlDataReader.GetInt32(4);
                    oTo.NumeroDocumento = sqlDataReader.GetString(5);
                    oTo.ApellidoPaterno = sqlDataReader.GetString(6);
                    oTo.ApellidoMaterno = sqlDataReader.GetString(7);
                    oTo.PrimerNombre = sqlDataReader.GetString(8);
                    oTo.SegundoNombre = sqlDataReader.GetString(9);
                    oLista.Add(oTo);
                }
            }
            catch (Exception ex)
            {
                _xError = ex.Message;
                Comun.EventosGrabar("MCPFichaMedica", "DBPersonalContratista.ListarPersonalContratista", ex);
            }
            finally
            {
                sqlDataReader?.Dispose();
                sqlCommand.Dispose();
                dbConexion.Dispose();
            }

            return (oLista);
        }
        public Collection<TOBaseI> ListarEmpresas()
        {
            DBConexionPersonalContratista dbConexion = new DBConexionPersonalContratista();
            SqlDataReader sqlDataReader = null;
            SqlCommand sqlCommand = new SqlCommand();
            Collection<TOBaseI> oLista = new Collection<TOBaseI>();

            _xError = "";

            try
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "que_TbTerceroEmpresas_Listar";

                sqlCommand.Connection = dbConexion.AbrirModoLectura();
                sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    TOBaseI oTo = new TOBaseI();
                    oTo.Key = sqlDataReader.GetInt32(0);
                    oTo.Nombre = sqlDataReader.GetString(1);

                    oLista.Add(oTo);
                }
            }
            catch (Exception ex)
            {
                _xError = ex.Message;
                Comun.EventosGrabar("MCPFichaMedica", "DBPersonalContratista.ListarEmpresas", ex);
            }
            finally
            {
                sqlDataReader?.Dispose();
                sqlCommand.Dispose();
                dbConexion.Dispose();
            }
            return (oLista);
        }
        public Collection<TOBaseI> ListarTiposDocumento()
        {
            DBConexionPersonalContratista dbConexion = new DBConexionPersonalContratista();
            SqlDataReader sqlDataReader = null;
            SqlCommand sqlCommand = new SqlCommand();
            Collection<TOBaseI> oLista = new Collection<TOBaseI>();

            _xError = "";

            try
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "que_TipoDocumento_Listar";

                sqlCommand.Connection = dbConexion.AbrirModoLectura();
                sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    TOBaseI oTo = new TOBaseI();
                    oTo.Key = sqlDataReader.GetInt32(0);
                    oTo.Nombre = sqlDataReader.GetString(1);

                    oLista.Add(oTo);
                }
            }
            catch (Exception ex)
            {
                _xError = ex.Message;
                Comun.EventosGrabar("MCPFichaMedica", "DBPersonalContratista.ListarTiposDocumento", ex);
            }
            finally
            {
                sqlDataReader?.Dispose();
                sqlCommand.Dispose();
                dbConexion.Dispose();
            }
            return (oLista);
        }
        public bool RegistrarPersonalContratista(TOPersonalContratista pPersonal, string pCodUsuario)
        {
            DBConexionPersonalContratista dbConexion = new DBConexionPersonalContratista();
            SqlCommand oSqlCmd = new SqlCommand();
            bool xRes = false;

            _xError = "";

            try
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.CommandText = "ins_PersonalContratista_Registrar";
                oSqlCmd.Parameters.AddWithValue("@inPersonalContratista", pPersonal.Contratista.Key);
                oSqlCmd.Parameters.AddWithValue("@inEmpresa", pPersonal.Empresa.Key);
                oSqlCmd.Parameters.AddWithValue("@inTipoDocumento", pPersonal.TipoDocumento.Key);
                oSqlCmd.Parameters.AddWithValue("@vcNumeroDocumento", pPersonal.NumeroDocumento);
                oSqlCmd.Parameters.AddWithValue("@vcApellidoPaterno", pPersonal.ApellidoPaterno);
                oSqlCmd.Parameters.AddWithValue("@vcApellidoMaterno", pPersonal.ApellidoMaterno);
                oSqlCmd.Parameters.AddWithValue("@vcPrimerNombre", pPersonal.PrimerNombre);
                oSqlCmd.Parameters.AddWithValue("@vcSegundoNombre", pPersonal.SegundoNombre);
                oSqlCmd.Parameters.AddWithValue("@vcUsuario", pCodUsuario);

                oSqlCmd.Connection = dbConexion.AbrirModoEscritura();
                oSqlCmd.ExecuteNonQuery();

                xRes = true;
            }
            catch (Exception ex)
            {
                xRes = false;
                _xError = ex.Message;

                Comun.EventosGrabar("MCPFichaMedica", "DBPersonalContratista.RegistrarPersonalContratista", ex);
            }
            finally
            {
                oSqlCmd.Dispose();
                dbConexion.Dispose();
            }

            return (xRes);
        }
        public bool EsValidoNumeroDocumento(TOPersonalContratista pPersonal)
        {
            DBConexionPersonalContratista dbConexion = new DBConexionPersonalContratista();
            SqlCommand oSqlCmd = new SqlCommand();
            bool xRes = false;

            _xError = "";

            try
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.CommandText = "que_NumeroDocumentoPersonalContratista_Consultar";
                oSqlCmd.Parameters.AddWithValue("@vcNumeroDocumento", pPersonal.NumeroDocumento);
                oSqlCmd.Parameters.AddWithValue("@inEmpresa", pPersonal.Empresa.Key);

                oSqlCmd.Connection = dbConexion.AbrirModoEscritura();
                oSqlCmd.ExecuteNonQuery();

                var resultado = oSqlCmd.ExecuteScalar();

                bool resultadoBooleano = (bool)(resultado);

                xRes = resultadoBooleano;
            }
            catch (Exception ex)
            {
                xRes = false;
                _xError = ex.Message;

                Comun.EventosGrabar("MCPFichaMedica", "DBPersonalContratista.EsValidoNumeroDocumento", ex);
            }
            finally
            {
                oSqlCmd.Dispose();
                dbConexion.Dispose();
            }

            return (xRes);
        }
    }
}
