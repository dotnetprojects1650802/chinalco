﻿
using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Web.Configuration;


using Chinalco.Shared;

namespace Chinalco.RRHH.Salud
{
    internal class DBConexionImportarPersonalContratista : Component
    {
        private string xSqlRead = "";
        private string xSqlWrite = "";
        private string xSrvApps = "";
        private string xSrvDB = "BD_Terceros";
        private string mUserR = "User Id=dbRHSO;Password=dbRHSO;";
        private string mUserW = "User Id=dbRHSO;Password=dbRHSO;";
        private SqlConnection oCn = new SqlConnection();
        private Container components;

        public DBConexionImportarPersonalContratista(IContainer container)
        {
            container.Add((IComponent)this);
            this.InitializeComponent();
            this.xSrvApps = this.SQLServerName();
        }

        public DBConexionImportarPersonalContratista()
        {
            this.InitializeComponent();
            this.xSrvApps = this.SQLServerName();
        }

        public SqlConnection AbrirModoLectura()
        {
            this.oCn = new SqlConnection();

            if (this.xSrvApps != "")
            {
                this.xSqlRead = "Data Source=" + this.xSrvApps + ";Initial Catalog=" + this.xSrvDB + ";" + this.mUserR + "Connection Timeout=50;";
                this.oCn.ConnectionString = this.xSqlRead;
                try
                {
                    this.oCn.Open();
                }
                catch (Exception ex)
                {
                    Comun.EventosGrabar("MCPFichaMedica", "DBConexion.AbrirModoLectura", ex);
                }
            }

            return this.oCn;
        }

        public SqlConnection AbrirModoEscritura()
        {
            this.oCn = new SqlConnection();
            this.xSqlWrite = "Data Source=" + this.xSrvApps + ";Initial Catalog=" + this.xSrvDB + ";" + this.mUserW + "Connection Timeout=30;";
            this.oCn.ConnectionString = this.xSqlWrite;
            try
            {
                this.oCn.Open();
            }
            catch (Exception ex)
            {
                Comun.EventosGrabar("MCPFichaMedica", "DBConexion.AbrirModoLectura", ex);
            }
            return this.oCn;
        }
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            if (this.oCn != null)
            {
                this.oCn.Close();
                this.oCn.Dispose();
            }
            base.Dispose(disposing);
        }

        private string SQLServerName() => WebConfigurationManager.AppSettings[nameof(SQLServerName)];
    }
}
