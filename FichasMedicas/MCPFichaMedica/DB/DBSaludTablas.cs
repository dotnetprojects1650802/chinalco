﻿

using Chinalco.RRHH.Salud.TO;
using Chinalco.Shared;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;

namespace Chinalco.RRHH.Salud.DB
{
  internal class DBSaludTablas
  {
    private string _xError = string.Empty;

        public string Error()
        {
            return (this._xError);
        }

        public Collection<TOBaseI> ListarCatalogo()
        {
            DBConexionSalud dbConexion = new DBConexionSalud();
            SqlDataReader sqlDataReader = null;
            SqlCommand sqlCommand = new SqlCommand();
            Collection<TOBaseI> collection = new Collection<TOBaseI>();

            this._xError = "";

            try
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "que_TbCatalogo_Listar";
               
                sqlCommand.Connection = dbConexion.AbrirModoLectura();
                sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    TOBaseI tosmElemento = new TOBaseI();
                    tosmElemento.Key = sqlDataReader.GetInt32(0);
                    tosmElemento.Nombre = sqlDataReader[1].ToString();

                    collection.Add(tosmElemento);
                }
            }
            catch (Exception ex)
            {
                this._xError = ex.Message;
                Comun.EventosGrabar("MCPRRHH", "DBSaludTablas.ListarCatalogo", ex);
            }
            finally
            {
                sqlDataReader?.Dispose();
                sqlCommand.Dispose();
                dbConexion.Dispose();
            }
            return collection;
        }
        public Collection<TOSaludCatalogoItem> ListarElementosCatalogo(int pCatalogo)
        {
          DBConexionSalud dbConexion = new DBConexionSalud();
          SqlDataReader sqlDataReader = null;
          SqlCommand sqlCommand = new SqlCommand();
          Collection<TOSaludCatalogoItem> collection = new Collection<TOSaludCatalogoItem>();

          this._xError = "";

          try
          {
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "que_TbCatalogoElemento_Consultar";
                    sqlCommand.Parameters.AddWithValue("@inCatalogo", pCatalogo);
            sqlCommand.Connection = dbConexion.AbrirModoLectura();
            sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                    TOSaludCatalogoItem tosmElemento = new TOSaludCatalogoItem();
                tosmElemento.Key = sqlDataReader.GetInt32(0);
                tosmElemento.Nombre = sqlDataReader[1].ToString();
                    tosmElemento.RegistradoPor = sqlDataReader.GetString(2);
                    tosmElemento.FechaRegistro = sqlDataReader.GetString(3);

                    collection.Add(tosmElemento);
            }
          }
          catch (Exception ex)
          {
            this._xError = ex.Message;
            Comun.EventosGrabar("MCPRRHH", "DBSaludTablas.ListarElementosCatalogo", ex);
          }
          finally
          {
            sqlDataReader?.Dispose();
            sqlCommand.Dispose();
            dbConexion.Dispose();
          }
          return collection;
        }

        public Collection<TOBaseI> ListarMedicos(int pClinica)
        {
            DBConexionSalud dbConexion = new DBConexionSalud();
            SqlDataReader sqlDataReader = null;
            SqlCommand sqlCommand = new SqlCommand();
            Collection<TOBaseI> collection = new Collection<TOBaseI>();

            this._xError = "";

            try
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "que_TbSMMedico_ListarPorClinica";
                sqlCommand.Parameters.AddWithValue("@inClinica", pClinica);
                sqlCommand.Connection = dbConexion.AbrirModoLectura();
                sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    TOBaseI tosmElemento = new TOBaseI();
                    tosmElemento.Key = sqlDataReader.GetInt32(0);
                    tosmElemento.Nombre = sqlDataReader[1].ToString();

                    collection.Add(tosmElemento);
                }
            }
            catch (Exception ex)
            {
                this._xError = ex.Message;
                Comun.EventosGrabar("MCPRRHH", "DBSaludTablas.ListarMedicos", ex);
            }
            finally
            {
                sqlDataReader?.Dispose();
                sqlCommand.Dispose();
                dbConexion.Dispose();
            }

            return collection;
        }

        public TOSaludColaborador BuscarColaborador(int pColaborador, int pEmpresa)
        {
            DBConexionSalud dbConexion = new DBConexionSalud();
            SqlDataReader sqlDataReader = null;
            SqlCommand sqlCommand = new SqlCommand();
            TOSaludColaborador oTo = new TOSaludColaborador();

            this._xError = "";

            try
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "que_Colaborador_Buscar";
                sqlCommand.Parameters.AddWithValue("@inColaborador", pColaborador);
                sqlCommand.Parameters.AddWithValue("@inEmpresa", pEmpresa);
                sqlCommand.Connection = dbConexion.AbrirModoLectura();
                sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                  
                    oTo.Key = sqlDataReader.GetInt32(0);
                    oTo.Nombre = sqlDataReader[1].ToString();
                    oTo.Empresa.Key = sqlDataReader.GetInt32(2);
                    oTo.Empresa.Nombre = sqlDataReader[3].ToString();
                    oTo.KeyResultado= sqlDataReader.GetInt32(4);

                }
            }
            catch (Exception ex)
            {
                this._xError = ex.Message;
                Comun.EventosGrabar("MCPRRHH", "DBSaludTablas.BuscarColaborador", ex);
            }
            finally
            {
                sqlDataReader?.Dispose();
                sqlCommand.Dispose();
                dbConexion.Dispose();
            }
            return (oTo);
        }

        public bool RegistrarElemento(TOSaludCatalogoItem pElemento, string pCodUsuario)
        {
            DBConexionSalud dbConexion = new DBConexionSalud();
            SqlCommand oSqlCmd = new SqlCommand();           
            bool xRes = false;
         
            this._xError = "";

            try
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.CommandText = "ins_TbCatalogoElemento_Registrar";
                oSqlCmd.Parameters.AddWithValue("@inCatalogo", pElemento.KeyCatalogo);
                oSqlCmd.Parameters.AddWithValue("@inElemento", pElemento.Key);
                oSqlCmd.Parameters.AddWithValue("@vcElemento", pElemento.Nombre);
                oSqlCmd.Parameters.AddWithValue("@vcUsuario", pCodUsuario);               
             
                oSqlCmd.Connection = dbConexion.AbrirModoEscritura();             
                oSqlCmd.ExecuteNonQuery();
               
                xRes = true;

            }
            catch (Exception ex)
            {
                xRes = false;
                this._xError = ex.Message;
              
                Comun.EventosGrabar("MCPRRHH", "DBSaludResultado.RegistrarElemento", ex);
            }
            finally
            {               
                oSqlCmd.Dispose();              
                dbConexion.Dispose();
            }

            return (xRes);
        }

    }
}
