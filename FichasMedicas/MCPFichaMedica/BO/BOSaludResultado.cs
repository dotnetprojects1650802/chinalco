﻿
using Chinalco.RRHH.Salud.DB;
using Chinalco.RRHH.Salud.TO;
using Chinalco.Shared;
using System;
using System.Collections.ObjectModel;

namespace Chinalco.RRHH.Salud.BO
{
    public class BOSaludResultado
  {
        private DBSaludResultado oDb = new DBSaludResultado();
        private Collection<TOError> errLista = new Collection<TOError>();

        public Collection<TOError> Errores()
        {
            return (this.errLista);
        }

        public Collection<TOSaludResultado> ListarResultados(string pTextoBusqueda)
        {
            return (oDb.ListarResultados(pTextoBusqueda));

        }

        public Collection<TOSaludEvaluacion> ListarEvaluaciones(int pResultadoMedico)
        {
            return (oDb.ListarEvaluaciones(pResultadoMedico));
        }

        public Collection<TOSaludEvaluacion> ListarEvaluacionesPorPersona(int pPersona)
        {
            return (oDb.ListarEvaluacionesPorPersona(pPersona));
        }
        public TOBinario BuscarExamen(int pEvaluacion)
        {
            return (oDb.BuscarArchivo(pEvaluacion,0));
        }

        public TOBinario BuscarPlacaFrontal(int pEvaluacion)
        {
            return (oDb.BuscarArchivo(pEvaluacion,1));
        }

        public TOBinario BuscarPlacaLateral(int pEvaluacion)
        {
            return (oDb.BuscarArchivo(pEvaluacion, 2));
        }

        public bool RegistrarResultado(TOSaludEvaluacion pEvaluacion, string pCodUsuario)
        {
            TOBinario oToArchivo = Comun.ArchivoDesdeBase64(pEvaluacion.Archivo.Nombre, pEvaluacion.Archivo.Data);
            TOBinario oToPlacaF = new TOBinario();
            TOBinario oToPlacaL = new TOBinario();
            DateTime xFecha = Comun.VerificarFecha(pEvaluacion.Fecha);
            int xAnio = 2000;
            bool xRes = false;
            
            errLista = new Collection<TOError>();
                       
            if (pEvaluacion.Colaborador.Key == 0) errLista.Add(new TOError(101, "Debe especificar los datos del Colaborador."));
            if (pEvaluacion.Clinica.Key == 0) errLista.Add(new TOError(102, "Debe especificar la Clínica."));
            if (pEvaluacion.EmpresaEvaluadora.Key == 0) errLista.Add(new TOError(103, "Debe especificar la Empresa Evaluadora."));
            if (pEvaluacion.Medico.Key == 0) errLista.Add(new TOError(104, "Debe especificar al Médico."));
            if (pEvaluacion.TipoExamen.Key == 0) errLista.Add(new TOError(105, "Debe especificar el Tipo de Exámen Médico."));
            if (pEvaluacion.TipoResultado.Key == 0) errLista.Add(new TOError(106, "Debe especificar el Tipo de Resultado."));
            if (xFecha.Year==1900) errLista.Add(new TOError(107, "Debe especificar la Fecha del Exámen Médico."));
            else
            {
                if (pEvaluacion.Anio < xAnio) errLista.Add(new TOError(107, "Debe especificar el Año de la Evaluación."));
                if (pEvaluacion.Anio > xFecha.Year) errLista.Add(new TOError(107, "Debe especificar el Año de la Evaluación."));
            }

            if (pEvaluacion.Key == 0)
            {
                if (oToArchivo.Tamanio == 0) errLista.Add(new TOError(108, "Debe especificar el archivo digital del Exámen Médico."));
            }

            if (pEvaluacion.PlacaF.Size > 0)
            {
                oToPlacaF=Comun.ArchivoDesdeBase64(pEvaluacion.PlacaF.Nombre, pEvaluacion.PlacaF.Data);

                if (oToPlacaF.Tamanio==0) errLista.Add(new TOError(108, "Error al leer el archivo de la Placa Radiográfica Frontal."));
            }

            if (pEvaluacion.PlacaL.Size > 0)
            {
                oToPlacaL = Comun.ArchivoDesdeBase64(pEvaluacion.PlacaL.Nombre, pEvaluacion.PlacaL.Data);

                if (oToPlacaL.Tamanio == 0) errLista.Add(new TOError(108, "Error al leer el archivo de la Placa Radiográfica Lateral."));
            }

            if (errLista.Count==0)
            {
                xRes = oDb.RegistrarResultado(pEvaluacion, oToArchivo, oToPlacaF, oToPlacaL , xFecha, pCodUsuario);

                if (!xRes) errLista.Add(new TOError(404, oDb.Error()));
            }

            return (xRes);
        }

        public bool BorrarEvaluacion(int pEvaluacion, string pCodUsuario)
        {
            bool xRes = false;

            xRes = oDb.BorrarEvaluacion(pEvaluacion, pCodUsuario);

            if (!xRes) errLista.Add(new TOError(404, oDb.Error()));

            return (xRes);
        }

  }
}
