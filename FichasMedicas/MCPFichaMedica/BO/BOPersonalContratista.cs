﻿
using Chinalco.RRHH.Salud.DB;
using Chinalco.RRHH.Salud.TO;
using Chinalco.Shared;
using System.Collections.ObjectModel;

namespace Chinalco.RRHH.Salud.BO
{
    public class BOPersonalContratista
    {
        private DBPersonalContratista oDb = new DBPersonalContratista();
        private Collection<TOError> errLista = new Collection<TOError>();
        public Collection<TOError> Errores()
        {
            return (errLista);
        }
        public Collection<TOPersonalContratista> ListarPersonalContratista(string pTextoBusqueda)
        {
            return (oDb.ListarPersonalContratista(pTextoBusqueda));
        }
        public Collection<TOBaseI> ListarEmpresas()
        {
            return (oDb.ListarEmpresas());
        }
        public Collection<TOBaseI> ListarTiposDocumento()
        {
            return (oDb.ListarTiposDocumento());
        }
        public bool RegistrarPersonalContratista(TOPersonalContratista pDatos, string pCodUsuario)
        {
            bool xRes = false;

            errLista = new Collection<TOError>();

            if (pDatos.Empresa.Key == 0) errLista.Add(new TOError(101, "Debería especificar una Empresa."));
            if (pDatos.TipoDocumento.Key == 0) errLista.Add(new TOError(102, "Debería especificar el Tipo de Documento."));
            if (string.IsNullOrEmpty(pDatos.NumeroDocumento)) errLista.Add(new TOError(103, "Debería especificar el Número de Documento."));
            if (pDatos.EstadoOperacion.Equals("add")
                && !string.IsNullOrEmpty(pDatos.NumeroDocumento) && pDatos.Empresa.Key != 0
                && oDb.EsValidoNumeroDocumento(pDatos)) errLista.Add(new TOError(104, "El Número de Documento ingresado ya existe para la Empresa."));
            if (pDatos.NumeroDocumento.Length > 20) errLista.Add(new TOError(105, "El Número de Documento no debería exceder de los 20 caracteres."));
            if (string.IsNullOrEmpty(pDatos.ApellidoPaterno)) errLista.Add(new TOError(106, "Debería especificar el Apellido Paterno."));
            if (string.IsNullOrEmpty(pDatos.ApellidoMaterno)) errLista.Add(new TOError(107, "Debería especificar el Apellido Materno."));
            if (string.IsNullOrEmpty(pDatos.PrimerNombre)) errLista.Add(new TOError(108, "Debería especificar el Primer Nombre."));
            if (errLista.Count == 0)
            {
                xRes = oDb.RegistrarPersonalContratista(pDatos, pCodUsuario);

                if (!xRes) errLista.Add(new TOError(404, oDb.Error()));
            }

            return (xRes);
        }
    }
}
