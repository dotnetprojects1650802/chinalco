﻿
using System.Collections.ObjectModel;

using Chinalco.RRHH.Salud.DB;
using Chinalco.RRHH.Salud.TO;
using Chinalco.Shared;


namespace Chinalco.RRHH.Salud.BO
{
  public class BOSaludTablas
  {
    private DBSaludTablas oDb = new DBSaludTablas();
    private Collection<TOError> errLista = new Collection<TOError>();

        public Collection<TOError> Errores()
        {
            return (this.errLista);
        }

        public Collection<TOBaseI> ListarCatalogo()
        {
            return (oDb.ListarCatalogo());
        }
        public Collection<TOSaludCatalogoItem> ListarElementosCatalogo(int pCatalogo)
        {
            return (oDb.ListarElementosCatalogo(pCatalogo));

        }

        public Collection<TOBaseI> ListarMedicos(int pClinica)
        {
            return (oDb.ListarMedicos(pClinica));
        }
        public TOSaludColaborador BuscarColaborador(int pColaborador, int pEmpresa)
        {
            return (oDb.BuscarColaborador(pColaborador, pEmpresa));
        }

        public bool RegistrarElemento(TOSaludCatalogoItem pElemento, string pLogin)
        {
            bool xRes = false;

            errLista = new Collection<TOError>();

            if (pElemento.KeyCatalogo == 0) errLista.Add(new TOError(101, "Debe especificar el Catálogo."));
            if (pElemento.Nombre.Trim().Length == 0) errLista.Add(new TOError(101, "Debe especificar el Nombre del Elemento."));

            if (errLista.Count==0)
            {
                xRes = oDb.RegistrarElemento(pElemento, pLogin);

                if (!xRes) errLista.Add(new TOError(404, oDb.Error()));
            }

            return (xRes);
        }

  }
}
