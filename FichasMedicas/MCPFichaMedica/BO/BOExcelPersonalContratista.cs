﻿
using Chinalco.RRHH.Salud.DB;
using Chinalco.RRHH.Salud.TO;
using Chinalco.Shared;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;

namespace Chinalco.RRHH.Salud.BO
{
    public class BOExcelPersonalContratista
    {
        private DBImportarPersonalContratista oDb = new DBImportarPersonalContratista();
        private Collection<TOError> errLista = new Collection<TOError>();
        private static Collection<TODatosExcelSinProcesar> listaNoProcesados = new Collection<TODatosExcelSinProcesar>();
        public Collection<TOError> Errores()
        {
            return (errLista);
        }
        public bool RegistrarExcelPersonalContratista(TOImportarPersonalContratista pDatos, string pCodUsuario)
        {
            List<TODatosExcelPersonalContratista> xDatos = new List<TODatosExcelPersonalContratista>();
            TOBinario oToArchivo = Comun.ArchivoDesdeBase64(pDatos.ExcelPersonalContratista.Nombre, pDatos.ExcelPersonalContratista.Data);
            errLista = new Collection<TOError>();
            bool xRes = false;

            byte[] xExcelBytes = oToArchivo.Datos;

            xDatos = LeerDatosDesdeExcel(xExcelBytes);
            DataTable dtDatos = ConvertirListaADatatable(xDatos);


            if (errLista.Count == 0)
            {
                xRes = oDb.RegistrarExcelPersonalContratista(dtDatos, pCodUsuario);

                if (!xRes) errLista.Add(new TOError(404, oDb.Error()));
            }
            return (xRes);
        }

        public List<TODatosExcelPersonalContratista> LeerDatosDesdeExcel(byte[] archivoBytes)
        {
            List<TODatosExcelPersonalContratista> xDatos = new List<TODatosExcelPersonalContratista>();

            using (MemoryStream stream = new MemoryStream(archivoBytes))
            {
                using (ExcelPackage package = new ExcelPackage(stream))
                {
                    // Asumiendo que el archivo Excel tiene una sola hoja de trabajo (worksheet).
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[1];

                    // Iterar sobre las filas de la hoja de trabajo sin considerar la cabecera.
                    for (int row = 2; row <= worksheet.Dimension.Rows; row++)
                    {
                        if (worksheet.Cells[row, 1].Text.Equals("Contratista"))
                        {
                            if (oDb.EsValidoEmpresaRazonSocial(worksheet.Cells[row, 9].Text))
                            {
                                TODatosExcelPersonalContratista dato = CrearDatosDesdeFila(worksheet, row);
                                xDatos.Add(dato);
                            }
                            else
                            {
                                TODatosExcelSinProcesar dato = new TODatosExcelSinProcesar
                                {
                                    Index = row,
                                    Nombre = worksheet.Cells[row, 2].Text,
                                    TipoDOI = worksheet.Cells[row, 7].Text,
                                    NumeroDOI = worksheet.Cells[row, 8].Text,
                                    Empresa = worksheet.Cells[row, 9].Text
                                };
                                listaNoProcesados.Add(dato);
                            }
                        }
                    }
                }
            }

            return xDatos;
        }

        private TODatosExcelPersonalContratista CrearDatosDesdeFila(ExcelWorksheet worksheet, int row)
        {
            return new TODatosExcelPersonalContratista
            {
                Tipo = worksheet.Cells[row, 1].Text,
                Nombre = worksheet.Cells[row, 2].Text,
                Sexo = worksheet.Cells[row, 3].Text,
                FechaNacimiento = worksheet.Cells[row, 4].Text,
                Fotocheck = worksheet.Cells[row, 5].Text,
                FechaInicioProceso = worksheet.Cells[row, 6].Text,
                TipoDOI = worksheet.Cells[row, 7].Text,
                NumeroDOI = worksheet.Cells[row, 8].Text,
                Empresa = worksheet.Cells[row, 9].Text,
                Estado = worksheet.Cells[row, 10].Text,
                CodigoRFI = worksheet.Cells[row, 11].Text,
                Puesto = worksheet.Cells[row, 12].Text,
                Area = worksheet.Cells[row, 13].Text,
                CodigoContrato = worksheet.Cells[row, 14].Text,
                TipoContrato = worksheet.Cells[row, 15].Text,
                InicioContrato = worksheet.Cells[row, 16].Text,
                FinContrato = worksheet.Cells[row, 17].Text,
                AdministradorContrato = worksheet.Cells[row, 18].Text,
                DescripcionContrato = worksheet.Cells[row, 19].Text,
                Procedencia = worksheet.Cells[row, 20].Text,
                Departamento = worksheet.Cells[row, 21].Text,
                Provincia = worksheet.Cells[row, 22].Text,
                Distrito = worksheet.Cells[row, 23].Text,
                Direccion = worksheet.Cells[row, 24].Text,
                AdministradorContratos = worksheet.Cells[row, 25].Text,
                TipoManoDeObra = worksheet.Cells[row, 26].Text,
                LugarTrabajo = worksheet.Cells[row, 27].Text,
                CorreoElectronico = worksheet.Cells[row, 28].Text,
                Telefono = worksheet.Cells[row, 29].Text,
                CodigoUbigeo = worksheet.Cells[row, 30].Text,
                EstadoCivil = worksheet.Cells[row, 31].Text,
                NumeroHijos = worksheet.Cells[row, 32].Text,
                TelefonoContacto = worksheet.Cells[row, 33].Text,
                PersonaContacto = worksheet.Cells[row, 34].Text,
                NivelEstudios = worksheet.Cells[row, 35].Text,
                Profesion = worksheet.Cells[row, 36].Text,
                ApellidoPaterno = worksheet.Cells[row, 37].Text,
                ApellidoMaterno = worksheet.Cells[row, 38].Text,
                PrimerNombre = worksheet.Cells[row, 39].Text,
                SegundoNombre = worksheet.Cells[row, 40].Text,
            };
        }

        public Collection<TODatosExcelSinProcesar> ListaDatosExcelNoProcesados()
        {
            return listaNoProcesados;
        }

        public DataTable ConvertirListaADatatable(List<TODatosExcelPersonalContratista> listaDatos)
        {
            // Crear una nueva DataTable
            DataTable dataTable = new DataTable();

            // Obtener las propiedades de la clase TOImportarPersonalContratista
            var propiedades = typeof(TODatosExcelPersonalContratista).GetProperties();

            // Agregar columnas a la DataTable
            foreach (var propiedad in propiedades)
            {
                dataTable.Columns.Add(propiedad.Name, propiedad.PropertyType);
            }

            // Agregar filas a la DataTable
            foreach (var item in listaDatos)
            {
                DataRow fila = dataTable.NewRow();

                foreach (var propiedad in propiedades)
                {
                    fila[propiedad.Name] = propiedad.GetValue(item);
                }

                dataTable.Rows.Add(fila);
            }

            return dataTable;
        }
    }
}
